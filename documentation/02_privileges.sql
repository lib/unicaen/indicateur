INSERT INTO unicaen_privilege_categorie (code, libelle, ordre, namespace)
    VALUES ('indicateur', 'Gestions des indicateurs', 800, 'UnicaenIndicateur\Provider\Privilege');
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
WITH d(code, lib, ordre) AS (
    SELECT 'afficher_indicateur', 'Afficher un indicateur', 1   UNION
    SELECT 'editer_indicateur', 'Éditer un indicateur', 2   UNION
    SELECT 'detruire_indicateur', 'Effacer un indicateur', 3 UNION
    SELECT 'indicateur_index', 'Accéder à l''index', 10 UNION
    SELECT 'indicateur_mes_indicateurs', 'Affichage du menu - Mes Indicateurs -', 100
)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
JOIN unicaen_privilege_categorie cp ON cp.CODE = 'indicateur';

INSERT INTO unicaen_privilege_categorie (code, libelle, ordre, namespace)
VALUES ('abonnement', 'Gestions des abonnement', 810, 'UnicaenIndicateur\Provider\Privilege');
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
WITH d(code, lib, ordre) AS (
    SELECT 'afficher_abonnement', 'Afficher un abonnement', 4   UNION
    SELECT 'editer_abonnement', 'Éditer un abonnement', 5   UNION
    SELECT 'detruire_abonnement', 'Effacer un abonnement', 6
)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
         JOIN unicaen_privilege_categorie cp ON cp.CODE = 'abonnement';


INSERT INTO unicaen_privilege_categorie (code, libelle, ordre, namespace)
VALUES ('tableaudebord', 'Gestions des abonnement', 810, 'UnicaenIndicateur\Provider\Privilege');
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
WITH d(code, lib, ordre) AS (
    SELECT 'afficher_tableaudebord', 'Afficher un tableau de bord', 4   UNION
    SELECT 'editer_tableaudebord', 'Éditer un tableau de bord', 5   UNION
    SELECT 'detruire_tableaudebord', 'Effacer un tableau de bord', 6
)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
JOIN unicaen_privilege_categorie cp ON cp.CODE = 'tableaudebord';