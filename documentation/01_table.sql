create table unicaen_indicateur_categorie
(
    id          serial
        constraint unicaen_indicateur_categorie_pk
            primary key,
    code        varchar(256)
        constraint unicaen_indicateur_categorie_pk_2
            unique,
    libelle     varchar(1024)        not null,
    ordre       integer default 9999 not null,
    description text
);

create table unicaen_indicateur_indicateur
(
    id                       serial
        constraint indicateur_pk
            primary key,
    titre                    varchar(256)  not null,
    description              varchar(2048),
    requete                  varchar(4096) not null,
    dernier_rafraichissement timestamp,
    view_id                  varchar(256),
    entity                   varchar(256),
    namespace                varchar(1024),
    code                     varchar(256)
        constraint unicaen_indicateur_indicateur_pk
            unique,
    nb_elements              integer,
    categorie_id             integer
        constraint uii_unicaen_indicateur_categorie_id_fk
            references unicaen_indicateur_categorie,
    perimetre                varchar(256)
);
create unique index indicateur_id_uindex
    on unicaen_indicateur_indicateur (id);



create table unicaen_indicateur_abonnement
(
    id serial constraint abonnement_pk primary key,
    user_id integer constraint indicateur_abonnement_user_id_fk references unicaen_utilisateur_user on delete cascade,
    indicateur_id integer constraint indicateur_abonnement_indicateur_definition_id_fk references unicaen_indicateur on delete cascade,
    frequence     varchar(256),
    dernier_envoi timestamp
);

create unique index abonnement_id_uindex on unicaen_indicateur_abonnement (id);
create unique index indicateur_id_uindex on unicaen_indicateur (id);

create table unicaen_indicateur_tableaudebord
(
    id serial constraint unicaen_indicateur_tableaudebord_pk primary key,
    titre varchar(1024) default 'Tableau de bord' not null,
    description text,
    namespace varchar(256),
    nb_column   integer       default 1                 not null
);

create table unicaen_indicateur_tableau_indicateur
(
    tableau_id    integer not null
        constraint unicaen_indicateur_tableau_indicateur_tableaudebord_null_fk
            references unicaen_indicateur_tableaudebord (id)
            on delete cascade,
    indicateur_id integer not null
        constraint unicaen_indicateur_tableau_indicateur_indicateur_null_fk
            references unicaen_indicateur_indicateur (id)
            on delete cascade,
    constraint unicaen_indicateur_tableau_indicateur_pk
        primary key (tableau_id, indicateur_id)
);

