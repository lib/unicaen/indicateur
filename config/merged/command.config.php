<?php


use UnicaenIndicateur\Command\NotifyIndicateurCommand;
use UnicaenIndicateur\Command\NotifyIndicateurCommandFactory;
use UnicaenIndicateur\Command\NotifyIndicateursCommand;
use UnicaenIndicateur\Command\NotifyIndicateursCommandFactory;
use UnicaenIndicateur\Command\RefreshIndicateurCommand;
use UnicaenIndicateur\Command\RefreshIndicateurCommandFactory;
use UnicaenIndicateur\Command\RefreshIndicateursCommand;
use UnicaenIndicateur\Command\RefreshIndicateursCommandFactory;

return [
    'bjyauthorize' => [
        'guards' => [
        ],
    ],

    'laminas-cli' => [
        'commands' => [
            'notify-indicateur' => NotifyIndicateurCommand::class,
            'notify-indicateurs' => NotifyIndicateursCommand::class,
            'refresh-indicateur' => RefreshIndicateurCommand::class,
            'refresh-indicateurs' => RefreshIndicateursCommand::class,
        ],
    ],

    'controllers' => [
        'factories' => [
        ],
    ],
    'service_manager' => [
        'factories' => [
            NotifyIndicateurCommand::class => NotifyIndicateurCommandFactory::class,
            NotifyIndicateursCommand::class => NotifyIndicateursCommandFactory::class,
            RefreshIndicateurCommand::class => RefreshIndicateurCommandFactory::class,
            RefreshIndicateursCommand::class => RefreshIndicateursCommandFactory::class,
        ],
    ],

    'form_elements' => [
        'factories' => [
        ],
    ],
    'hydrators' => [
        'factories' => [
        ],
    ],
    'view_helpers' => [
        'invokables' => [
        ],
    ],
];

