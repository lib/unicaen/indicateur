<?php

use UnicaenIndicateur\Controller\IndexController;
use UnicaenIndicateur\Controller\IndexControllerFactory;
use UnicaenIndicateur\Provider\Privilege\IndicateurPrivileges;
use UnicaenPrivilege\Guard\PrivilegeController;
use Laminas\Router\Http\Literal;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => IndexController::class,
                    'action' => [
                        'index',
                        'abonnement'
                    ],
                    'privileges' => [
                        IndicateurPrivileges::INDICATEUR_MES_INDICATEURS,
                    ],
                ],
            ],

        ],
    ],


    'router'          => [
        'routes' => [
            'mes-indicateurs' => [
                'type'  => Literal::class,
                'options' => [
                    'route'    => '/mes-indicateurs',
                    'defaults' => [
                        'controller' => IndexController::class,
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'abonnement' => [
                        'type'  => Literal::class,
                        'options' => [
                            'route'    => '/abonnement',
                            'defaults' => [
                                'controller' => IndexController::class,
                                'action'     => 'abonnement',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
        ],
    ],
    'controllers'     => [
        'factories' => [
            IndexController::class => IndexControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
        ],
    ],
    'hydrators' => [
        'factories' => [
        ],
    ]

];