<?php

use UnicaenIndicateur\Controller\IndicateurController;
use UnicaenIndicateur\Controller\IndicateurControllerFactory;
use UnicaenIndicateur\Form\Indicateur\IndicateurForm;
use UnicaenIndicateur\Form\Indicateur\IndicateurFormFactory;
use UnicaenIndicateur\Form\Indicateur\IndicateurHydrator;
use UnicaenIndicateur\Form\Indicateur\IndicateurHydratorFactory;
use UnicaenIndicateur\Provider\Privilege\IndicateurPrivileges;
use UnicaenIndicateur\Service\Indicateur\IndicateurService;
use UnicaenIndicateur\Service\Indicateur\IndicateurServiceFactory;
use UnicaenIndicateur\View\Helper\DefaultItemViewHelper;
use UnicaenIndicateur\View\Helper\IndicateurViewHelperFactory;
use UnicaenPrivilege\Guard\PrivilegeController;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => IndicateurController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        IndicateurPrivileges::INDICATEUR_INDEX,
                    ],
                ],
                [
                    'controller' => IndicateurController::class,
                    'action' => [
                        'creer',
                        'modifier',
                    ],
                    'privileges' => [
                        IndicateurPrivileges::EDITER_INDICATEUR,
                    ],
                ],
                [
                    'controller' => IndicateurController::class,
                    'action' => [
                        'afficher',
                        'rafraichir',
                        'exporter',
                        'regenerer',
                    ],
                    'privileges' => [
                        IndicateurPrivileges::AFFICHER_INDICATEUR,
                    ],
                ],
                [
                    'controller' => IndicateurController::class,
                    'action' => [
                        'detruire',
                    ],
                    'privileges' => [
                        IndicateurPrivileges::DETRUIRE_INDICATEUR,
                    ],
                ],
            ],
        ],
    ],

    'router'          => [
        'routes' => [
            'indicateurs' => [
                'type'  => Literal::class,
                'options' => [
                    'route'    => '/indicateurs',
                    'defaults' => [
                        /** @see IndicateurController::indexAction() */
                        'controller' => IndicateurController::class,
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
            ],
            'indicateur' => [
                'type'  => Literal::class,
                'options' => [
                    'route'    => '/indicateur',
                    'defaults' => [
                        /** @see IndicateurController::indexAction() */
                        'controller' => IndicateurController::class,
                    ],
                ],
                'may_terminate' => false,
                'child_routes' => [
                    'afficher' => [
                        'type'  => Segment::class,
                        'options' => [
                            'route'    => '/afficher/:indicateur',
                            'defaults' => [
                                /** @see IndicateurController::afficherAction() */
                                'action'     => 'afficher',
                            ],
                        ],
                        'may_terminate' => true,
                    ],
                    'rafraichir' => [
                        'type'  => Segment::class,
                        'options' => [
                            'route'    => '/rafraichir/:indicateur',
                            'defaults' => [
                                /** @see IndicateurController::rafraichirAction() */
                                'action'     => 'rafraichir',
                            ],
                        ],
                        'may_terminate' => true,
                    ],
                    'regenerer' => [
                        'type'  => Segment::class,
                        'options' => [
                            'route'    => '/regenerer/:indicateur',
                            'defaults' => [
                                /** @see IndicateurController::regenererAction() */
                                'action'     => 'regenerer',
                            ],
                        ],
                        'may_terminate' => true,
                    ],
                    'creer' => [
                        'type'  => Literal::class,
                        'options' => [
                            'route'    => '/creer',
                            'defaults' => [
                                /** @see IndicateurController::creerAction() */
                                'action'     => 'creer',
                            ],
                        ],
                        'may_terminate' => true,
                    ],
                    'modifier' => [
                        'type'  => Segment::class,
                        'options' => [
                            'route'    => '/modifier/:indicateur',
                            'defaults' => [
                                /** @see IndicateurController::modifierAction() */
                                'action'     => 'modifier',
                            ],
                        ],
                        'may_terminate' => true,
                    ],
                    'detruire' => [
                        'type'  => Segment::class,
                        'options' => [
                            'route'    => '/detruire/:indicateur',
                            'defaults' => [
                                /** @see IndicateurController::detruireAction() */
                                'action'     => 'detruire',
                            ],
                        ],
                        'may_terminate' => true,
                    ],
                    'exporter' => [
                        'type'  => Segment::class,
                        'options' => [
                            'route'    => '/exporter/:indicateur',
                            'defaults' => [
                                /** @see IndicateurController::exporterAction() */
                                'action'     => 'exporter',
                            ],
                        ],
                        'may_terminate' => true,
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            IndicateurService::class => IndicateurServiceFactory::class,
        ],
    ],
    'controllers'     => [
        'factories' => [
            IndicateurController::class => IndicateurControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            IndicateurForm::class => IndicateurFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            IndicateurHydrator::class => IndicateurHydratorFactory::class,
        ],
    ],
    'view_helpers' => [
        'invokables' => [
            'defaultItem' => DefaultItemViewHelper::class,
        ],
        'factories' => [
            'indicateur' => IndicateurViewHelperFactory::class,
        ],
    ],
];