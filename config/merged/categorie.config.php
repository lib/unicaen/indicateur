<?php

use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenIndicateur\Controller\CategorieController;
use UnicaenIndicateur\Controller\CategorieControllerFactory;
use UnicaenIndicateur\Form\Categorie\CategorieForm;
use UnicaenIndicateur\Form\Categorie\CategorieFormFactory;
use UnicaenIndicateur\Form\Categorie\CategorieHydrator;
use UnicaenIndicateur\Form\Categorie\CategorieHydratorFactory;
use UnicaenIndicateur\Provider\Privilege\IndicateurPrivileges;
use UnicaenIndicateur\Service\Categorie\CategorieService;
use UnicaenIndicateur\Service\Categorie\CategorieServiceFactory;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => CategorieController::class,
                    'action' => [
                        'index',
                        'ajouter',
                        'modifier',
                    ],
                    'privileges' => [
                        IndicateurPrivileges::EDITER_INDICATEUR,
                    ],
                ],
                [
                    'controller' => CategorieController::class,
                    'action' => [
                        'afficher',
                    ],
                    'privileges' => [
                        IndicateurPrivileges::AFFICHER_INDICATEUR,
                    ],
                ],
                [
                    'controller' => CategorieController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => [
                        IndicateurPrivileges::DETRUIRE_INDICATEUR,
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'indicateur' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/indicateur',
                ],
                'may_terminate' => false,
                'child_routes' => [
                    'categorie' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/categorie',
                            'defaults' => [
                                /** @see CategorieController::indexAction() */
                                'controller' => CategorieController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'afficher' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/afficher/:categorie',
                                    'defaults' => [
                                        /** @see CategorieController::afficherAction() */
                                        'action' => 'afficher',
                                    ],
                                ],
                            ],
                            'ajouter' => [
                                'type' => Literal::class,
                                'options' => [
                                    'route' => '/ajouter',
                                    'defaults' => [
                                        /** @see CategorieController::ajouterAction() */
                                        'action' => 'ajouter',
                                    ],
                                ],
                            ],
                            'modifier' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/modifier/:categorie',
                                    'defaults' => [
                                        /** @see CategorieController::modifierAction() */
                                        'action' => 'modifier',
                                    ],
                                ],
                            ],
                            'supprimer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/supprimer/:categorie',
                                    'defaults' => [
                                        /** @see CategorieController::supprimerAction() */
                                        'action' => 'supprimer',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            CategorieService::class => CategorieServiceFactory::class,
        ],
    ],
    'controllers' => [
        'factories' => [
            CategorieController::class => CategorieControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            CategorieForm::class => CategorieFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            CategorieHydrator::class => CategorieHydratorFactory::class,
        ],
    ],
];