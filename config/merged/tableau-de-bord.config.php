<?php

use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenIndicateur\Controller\TableauDeBordController;
use UnicaenIndicateur\Controller\TableauDeBordControllerFactory;
use UnicaenIndicateur\Form\TableauDeBord\TableauDeBordForm;
use UnicaenIndicateur\Form\TableauDeBord\TableauDeBordFormFactory;
use UnicaenIndicateur\Form\TableauDeBord\TableauDeBordHydrator;
use UnicaenIndicateur\Form\TableauDeBord\TableauDeBordHydratorFactory;
use UnicaenIndicateur\Provider\Privilege\IndicateurPrivileges;
use UnicaenIndicateur\Service\TableauDeBord\TableauDeBordService;
use UnicaenIndicateur\Service\TableauDeBord\TableauDeBordServiceFactory;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => TableauDeBordController::class,
                    'action' => [
                        'index',
                        'ajouter',
                        'modifier',
                        'ajouter-indicateur',
                        'retirer-indicateur',
                        'supprimer',
                        'afficher',
                    ],
                    'privileges' => [
                        IndicateurPrivileges::AFFICHER_INDICATEUR,
                    ],
                ],
            ],

        ],
    ],

    'router'          => [
        'routes' => [
            'tableau-de-bord' => [
                'type'  => Literal::class,
                'options' => [
                    'route'    => '/tableau-de-bord',
                    'defaults' => [
                        'controller' => TableauDeBordController::class,
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'ajouter' => [
                        'type'  => Literal::class,
                        'options' => [
                            'route'    => '/ajouter',
                            'defaults' => [
                                'controller' => TableauDeBordController::class,
                                'action'     => 'ajouter',
                            ],
                        ],
                    ],
                    'afficher' => [
                        'type'  => Segment::class,
                        'options' => [
                            'route'    => '/afficher/:tableau-de-bord',
                            'defaults' => [
                                'controller' => TableauDeBordController::class,
                                'action'     => 'afficher',
                            ],
                        ],
                    ],
                    'modifier' => [
                        'type'  => Segment::class,
                        'options' => [
                            'route'    => '/modifier/:tableau-de-bord',
                            'defaults' => [
                                'controller' => TableauDeBordController::class,
                                'action'     => 'modifier',
                            ],
                        ],
                    ],
                    'supprimer' => [
                        'type'  => Segment::class,
                        'options' => [
                            'route'    => '/supprimer/:tableau-de-bord',
                            'defaults' => [
                                'controller' => TableauDeBordController::class,
                                'action'     => 'supprimer',
                            ],
                        ],
                    ],
                    'ajouter-indicateur' => [
                        'type'  => Segment::class,
                        'options' => [
                            'route'    => '/ajouter-indicateur/:tableau-de-bord',
                            'defaults' => [
                                'controller' => TableauDeBordController::class,
                                'action'     => 'ajouter-indicateur',
                            ],
                        ],
                    ],
                    'retirer-indicateur' => [
                        'type'  => Segment::class,
                        'options' => [
                            'route'    => '/retirer-indicateur/:tableau-de-bord/:indicateur',
                            'defaults' => [
                                'controller' => TableauDeBordController::class,
                                'action'     => 'retirer-indicateur',
                            ],
                        ],
                    ],

                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            TableauDeBordService::class => TableauDeBordServiceFactory::class,
        ],
    ],
    'controllers'     => [
        'factories' => [
            TableauDeBordController::class => TableauDeBordControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            TableauDeBordForm::class => TableauDeBordFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            TableauDeBordHydrator::class => TableauDeBordHydratorFactory::class,
        ],
    ]

];