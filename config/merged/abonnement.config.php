<?php

use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenIndicateur\Controller\AbonnementController;
use UnicaenIndicateur\Controller\AbonnementControllerFactory;
use UnicaenIndicateur\Provider\Privilege\AbonnementPrivileges;
use UnicaenIndicateur\Service\Abonnement\AbonnementService;
use UnicaenIndicateur\Service\Abonnement\AbonnementServiceFactory;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => AbonnementController::class,
                    'action' => [
                        'souscrire',
                        'resilier',
                        'notifier'
                    ],
                    'privileges' => [
                        AbonnementPrivileges::EDITER_ABONNEMENT,
                    ],
                ],
                [
                    'controller' => AbonnementController::class,
                    'action' => [
                        'notifier-console'
                    ],
                    'roles' => [],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'abonnement' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/abonnement',
                    'defaults' => [
                        'controller' => AbonnementController::class,
                    ],
                ],
                'may_terminate' => false,
                'child_routes' => [
                    'souscrire' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/souscrire/:indicateur',
                            'defaults' => [
                                /** @see AbonnementController::souscrireAction() */
                                'action' => 'souscrire'
                            ],
                        ],
                        'may_terminate' => true,
                    ],
                    'resilier' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/resilier/:indicateur',
                            'defaults' => [
                                /** @see AbonnementController::resilierAction() */
                                'action' => 'resilier'
                            ],
                        ],
                        'may_terminate' => true,
                    ],
                    'notifier' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/notifier',
                            'defaults' => [
                                /** @see AbonnementController::notifierAction() */
                                'action' => 'notifier'
                            ],
                        ],
                        'may_terminate' => true,
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            AbonnementService::class => AbonnementServiceFactory::class,
        ],
    ],
    'controllers' => [
        'factories' => [
            AbonnementController::class => AbonnementControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [],
    ],
    'hydrators' => [
        'factories' => [],
    ]

];