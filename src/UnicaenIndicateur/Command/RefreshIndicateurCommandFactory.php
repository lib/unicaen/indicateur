<?php

namespace UnicaenIndicateur\Command;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\Console\Command\Command;
use UnicaenIndicateur\Service\Indicateur\IndicateurService;

class RefreshIndicateurCommandFactory extends Command
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): RefreshIndicateurCommand
    {
        /**
         * @see IndicateurService $indicateurService
         */
        $indcateurService = $container->get(IndicateurService::class);

        $command = new RefreshIndicateurCommand();
        $command->setIndicateurService($indcateurService);
        return $command;
    }
}
