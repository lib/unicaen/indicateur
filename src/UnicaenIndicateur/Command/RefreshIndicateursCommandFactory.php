<?php

namespace UnicaenIndicateur\Command;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\Console\Command\Command;
use UnicaenIndicateur\Service\Indicateur\IndicateurService;

class RefreshIndicateursCommandFactory extends Command
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): RefreshIndicateursCommand
    {
        /**
         * @see IndicateurService $indicateurService
         */
        $indcateurService = $container->get(IndicateurService::class);

        $command = new RefreshIndicateursCommand();
        $command->setIndicateurService($indcateurService);
        return $command;
    }
}
