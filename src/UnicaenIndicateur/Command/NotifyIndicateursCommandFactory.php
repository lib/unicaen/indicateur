<?php

namespace UnicaenIndicateur\Command;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\Console\Command\Command;
use UnicaenIndicateur\Service\Abonnement\AbonnementService;
use UnicaenIndicateur\Service\Indicateur\IndicateurService;
use UnicaenMail\Service\Mail\MailService;

class NotifyIndicateursCommandFactory extends Command
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): NotifyIndicateursCommand
    {
        /**
         * @see AbonnementService $abonnementService
         * @see IndicateurService $indicateurService
         * @see MailService $mailService
         */
        $abonnementService = $container->get(AbonnementService::class);
        $indcateurService = $container->get(IndicateurService::class);
        $mailService = $container->get(MailService::class);

        $command = new NotifyIndicateursCommand();
        $command->setAbonnementService($abonnementService);
        $command->setIndicateurService($indcateurService);
        $command->setMailService($mailService);
        return $command;
    }
}
