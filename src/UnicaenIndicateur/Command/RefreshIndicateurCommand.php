<?php

namespace UnicaenIndicateur\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use UnicaenIndicateur\Service\Indicateur\IndicateurServiceAwareTrait;

class RefreshIndicateurCommand extends Command
{
    use IndicateurServiceAwareTrait;

    protected static $defaultName = 'refresh-indicateur';

    protected function configure(): void
    {
        $this->setDescription("Rafraichissement d'un indicateur (dont le code est passé en argument)");
        $this->addArgument('code', InputArgument::REQUIRED, "Code de l'indicateur");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $code = $input->getArgument('code');

        $io->title("Rafraichissement de l'indicateur [" . $code . "]");

        $indicateur = $this->getIndicateurService()->getIndicateurByCode($code);

        if ($indicateur === null) {
            $io->error("Aucun indicateur pour ce code [" . $code . "]");
            return self::FAILURE;
        }

        $this->getIndicateurService()->refresh($indicateur);

        $io->success("Rafraichissements de l'indicateur effectué");
        return self::SUCCESS;
    }
}