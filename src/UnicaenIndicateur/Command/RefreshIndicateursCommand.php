<?php

namespace UnicaenIndicateur\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use UnicaenIndicateur\Service\Indicateur\IndicateurServiceAwareTrait;

class RefreshIndicateursCommand extends Command
{
    use IndicateurServiceAwareTrait;

    protected static $defaultName = 'refresh-indicateurs';

    protected function configure(): void
    {
        $this->setDescription("Rafraichissement des indicateurs");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->title("Rafraichissement des indicateurs");

        $indicateurs = $this->getIndicateurService()->getIndicateurs();
        $nbIndicateurs = count($indicateurs);
        $io->text($nbIndicateurs . " indicateurs à rafraichir");

        $position = 1;
        foreach ($indicateurs as $indicateur) {
            $io->text($position . "/" . $nbIndicateurs . " : indicateur [" . $indicateur->getCode() . "]");
            $this->getIndicateurService()->refresh($indicateur);
            $position++;
        }

        $io->success("Rafraichissements des indicateurs effectués");
        return self::SUCCESS;
    }
}