<?php

namespace UnicaenIndicateur\Command;

use DateTime;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use UnicaenIndicateur\Service\Abonnement\AbonnementServiceAwareTrait;
use UnicaenIndicateur\Service\Indicateur\IndicateurServiceAwareTrait;
use UnicaenMail\Service\Mail\MailServiceAwareTrait;

class NotifyIndicateurCommand extends Command
{
    use AbonnementServiceAwareTrait;
    use IndicateurServiceAwareTrait;
    use MailServiceAwareTrait;

    protected static $defaultName = 'notify-indicateur';

    protected function configure(): void
    {
        $this->setDescription("Notification d'un indicateur aux abonné·es");
        $this->addArgument('code', InputArgument::REQUIRED, "Code de l'indicateur");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $code = $input->getArgument('code');

        $io->title("Notification de l'indicateur [" . $code . "] aux abonné·es");

        $indicateur = $this->getIndicateurService()->getIndicateurByCode($code);

        if ($indicateur === null) {
            $io->error("Aucun indicateur pour ce code [" . $code . "]");
            return self::FAILURE;
        }

        $abonnements = $indicateur->getAbonnements();
        $io->text(count($abonnements) . " abonnements");

        $titre = "Publication de l'indicateur [" . $indicateur->getTitre() . "] (" . (new DateTime())->format("d/m/Y à H:i:s") . ")";
        $result = $this->getIndicateurService()->getIndicateurData($indicateur);
        $texte = "<table>";
        $texte .= "<thead>";
        $texte .= "<tr>";
        foreach ($result[0] as $rubrique) $texte .= "<th>" . $rubrique . "</th>";
        $texte .= "</tr>";
        $texte .= "</thead>";
        $texte .= "<tbody>";
        foreach ($result[1] as $item) {
            $texte .= "<tr>";
            foreach ($item as $value) $texte .= "<td>" . $value . "</td>";
            $texte .= "</tr>";
        }
        $texte .= "</tbody>";
        $texte .= "</table>";

        foreach ($abonnements as $abonnement) {
            $adresse = $abonnement->getUser()->getEmail();
            $mail = $this->getMailService()->sendMail($adresse, $titre, $texte);
            $mail->setMotsClefs([$indicateur->generateTag()]);
            $this->getMailService()->update($mail);
            $abonnement->setDernierEnvoi(new DateTime());
            $this->getAbonnementService()->update($abonnement);
        }


        $io->success("Notifications des abonnements de l'indicateur [" . $code . "] effectuées");
        return self::SUCCESS;
    }
}