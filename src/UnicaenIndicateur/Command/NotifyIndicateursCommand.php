<?php

namespace UnicaenIndicateur\Command;

use DateTime;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use UnicaenIndicateur\Service\Abonnement\AbonnementServiceAwareTrait;
use UnicaenIndicateur\Service\Indicateur\IndicateurServiceAwareTrait;
use UnicaenMail\Service\Mail\MailServiceAwareTrait;

class NotifyIndicateursCommand extends Command
{
    use AbonnementServiceAwareTrait;
    use IndicateurServiceAwareTrait;
    use MailServiceAwareTrait;

    protected static $defaultName = 'notify-indicateurs';

    protected function configure(): void
    {
        $this->setDescription("Notification des indicateurs aux abonné·es");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->title("Notification des indicateurs aux abonné·es");

        $indicateurs = $this->getIndicateurService()->getIndicateurs();
        $nbIndicateurs = count($indicateurs);
        $io->text($nbIndicateurs . " indicateurs à notifier");

        $position = 1;
        foreach ($indicateurs as $indicateur) {
            $io->text($position ."/". $nbIndicateurs. " : indicateur [".$indicateur->getCode()."]");

            $abonnements = $indicateur->getAbonnements();
            $io->text($position ."/". $nbIndicateurs. " : ".count($abonnements)." abonnements pour cet indicateur" );

            $titre = "Publication de l'indicateur [".$indicateur->getTitre()."] (". (new DateTime())->format("d/m/Y à H:i:s").")";
            $result = $this->getIndicateurService()->getIndicateurData($indicateur);
            $texte  = "<table>";
            $texte .= "<thead>";
            $texte .= "<tr>";
            foreach ($result[0] as $rubrique) $texte .= "<th>" . $rubrique . "</th>";
            $texte .= "</tr>";
            $texte .= "</thead>";
            $texte .= "<tbody>";
            foreach ($result[1] as $item) {
                $texte .="<tr>";
                foreach ($item as $value) $texte .="<td>". $value ."</td>";
                $texte .="</tr>";
            }
            $texte .= "</tbody>";
            $texte .= "</table>";

            foreach ($abonnements as $abonnement) {
                $adresse = $abonnement->getUser()->getEmail();
                $mail = $this->getMailService()->sendMail($adresse, $titre, $texte);
                $mail->setMotsClefs([$indicateur->generateTag()]);
                $this->getMailService()->update($mail);
                $abonnement->setDernierEnvoi(new DateTime());
                $this->getAbonnementService()->update($abonnement);
            }
            $position++;
        }

        $io->success("Notifications des abonnements des indicateurs effectuées");
        return self::SUCCESS;
    }
}