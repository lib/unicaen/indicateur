<?php

namespace UnicaenIndicateur\View\Helper;

use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\View\Resolver\TemplatePathStack;
use UnicaenIndicateur\Entity\Db\Indicateur;
use UnicaenIndicateur\Service\Indicateur\IndicateurServiceAwareTrait;

class IndicateurViewHelper extends AbstractHelper {
    use IndicateurServiceAwareTrait;

    /**
     * @param Indicateur $indicateur
     * @param array $options
     * @return \Laminas\View\Helper\Partial|string
     *
     * La clef $options['ViewHelper'] permet d'indiquer le ViewHelperpour réaliser l'affichage
     * attention celui-ci doit implementer l'interface ItemViewHelperInterface
     */
    public function __invoke(Indicateur $indicateur, array $options = [])
    {
        /** @var PhpRenderer $view */
        $view = $this->getView();
        $view->resolver()->attach(new TemplatePathStack(['script_paths' => [__DIR__ . "/partial"]]));

        $data = $this->getIndicateurService()->getIndicateurData($indicateur);
        return $view->partial('indicateur', ['indicateur' => $indicateur, 'data' => $data, 'options' => $options]);
    }
}