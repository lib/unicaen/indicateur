<?php

namespace UnicaenIndicateur\View\Helper;

use Laminas\View\Helper\AbstractHelper;

class DefaultItemViewHelper extends AbstractHelper implements ItemViewHelperInterface
{
    public function getHeader(array $data) : string
    {
        $headers = $data[0];
        $texte  = "<thead>";
        $texte .= "<tr>";
        foreach ($headers as $header) {
            $texte .= "<th>";
            $texte .= $header;
            $texte .= "</th>";
        }
        $texte .= "</tr>";
        $texte .= "</thead>";
        return $texte;
    }

    public function getItem(array $data, int $position) : string
    {
        $values = $data[1][$position];
        $texte  = "<tr>";
        foreach ($values as $value) {
            $texte .= "<td>";
            $texte .= $value;
            $texte .= "</td>";
        }
        $texte .= "</tr>";
        return $texte;
    }
}