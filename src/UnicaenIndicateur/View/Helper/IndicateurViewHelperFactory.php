<?php

namespace UnicaenIndicateur\View\Helper;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenIndicateur\Service\Indicateur\IndicateurService;

class IndicateurViewHelperFactory {

    /**
     * @param ContainerInterface $container
     * @return IndicateurViewHelper
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : IndicateurViewHelper
    {
        /**
         * @var IndicateurService $indicateurService
         */
        $indicateurService = $container->get(IndicateurService::class);

        $helper = new IndicateurViewHelper();
        $helper->setIndicateurService($indicateurService);
        return $helper;
    }
}