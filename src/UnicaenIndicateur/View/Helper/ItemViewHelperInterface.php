<?php

namespace UnicaenIndicateur\View\Helper;

interface ItemViewHelperInterface
{
    /** Retourne le header du tableau */
    public function getHeader(array $data) : string ;

    /** Retourne la line du tableau associée avec l'item à la position $position */
    public function getItem(array $data, int $position) : string ;
}