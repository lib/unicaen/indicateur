<?php

namespace UnicaenIndicateur\Service\Indicateur;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class IndicateurServiceFactory {

    /**
     * @param ContainerInterface $container
     * @return IndicateurService
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : IndicateurService
    {
        /**
         * @var EntityManager $entityManager
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        $service = new IndicateurService();
        $service->setObjectManager($entityManager);
        return $service;
    }
}