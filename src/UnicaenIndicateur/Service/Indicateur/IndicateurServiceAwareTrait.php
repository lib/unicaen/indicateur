<?php

namespace UnicaenIndicateur\Service\Indicateur;

trait IndicateurServiceAwareTrait {

    private IndicateurService $indicateurService;

    public function getIndicateurService() : IndicateurService
    {
        return $this->indicateurService;
    }

    public function setIndicateurService(IndicateurService $indicateurService) : void
    {
        $this->indicateurService = $indicateurService;
    }

}