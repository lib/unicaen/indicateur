<?php

namespace UnicaenIndicateur\Service\Indicateur;

use DateTime;
use Doctrine\DBAL\Driver\Exception as DBA_Driver_Exception;
use Doctrine\DBAL\Exception as DBA_Exception;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Exception;
use Laminas\Mvc\Controller\AbstractActionController;
use RuntimeException;
use UnicaenIndicateur\Entity\Db\Indicateur;
use UnicaenIndicateur\Service\Perimetre\PerimetreServiceInterface;

/**
 * @property EntityManager $objectManager
 */
class IndicateurService
{
    use ProvidesObjectManager;

    /** GESTION DES ENTITES ***********************************************************************************************/

    /**
     * @param Indicateur $indicateur
     * @return Indicateur
     */
    public function create(Indicateur $indicateur): Indicateur
    {
        $this->getObjectManager()->persist($indicateur);
        $this->getObjectManager()->flush($indicateur);
        if ($indicateur->getCode() === null) $indicateur->setCode($indicateur->getId());
        return $indicateur;
    }

    /**
     * @param Indicateur $indicateur
     * @return Indicateur
     */
    public function update(Indicateur $indicateur): Indicateur
    {
        $this->getObjectManager()->flush($indicateur);
        return $indicateur;
    }

    /**
     * @param Indicateur $indicateur
     * @return Indicateur
     */
    public function delete(Indicateur $indicateur): Indicateur
    {
        $this->getObjectManager()->remove($indicateur);
        $this->getObjectManager()->flush($indicateur);
        return $indicateur;
    }

    /** GESTION DES VUES **********************************************************************************************/

    public function verifierExistanceMaterializedView($viewname): bool
    {
        try {
            $sql = "SELECT EXISTS (SELECT FROM pg_matviews WHERE matviewname='" . $viewname . "')";
            $res = $this->getObjectManager()->getConnection()->executeQuery($sql, [], []);
            $tmp = $res->fetchAllAssociative();
        } catch (Exception $e) {
            return false;
        }
        return $tmp[0]['exists'];
    }

    /**
     * @param Indicateur $indicateur
     * @param ?array $userPerimetre (un tableau des ids associé aux élements dans le périmetre de l'utilisateur connecté)
     * @return array|null
     */
    public function fetch(Indicateur $indicateur, ?array $userPerimetre = null): ?array
    {

        $exist = $this->verifierExistanceMaterializedView($indicateur->getViewId());
        if ($exist === false) return null;


        $sql = "SELECT * FROM " . $indicateur->getViewId() . " WHERE 1=1";

        if ($indicateur->getPerimetre() !== null and $indicateur->getPerimetre() !== "aucun") {
            $perimetresActifs = explode(PerimetreServiceInterface::PERIMETRE_SEPARATOR, $indicateur->getPerimetre());

            foreach ($perimetresActifs as $perimetreActif) {
                if ($userPerimetre !== null) {
                    if (!empty($userPerimetre) AND isset($userPerimetre[$perimetreActif])) {
                        $sql .= " AND perimetre_" . $perimetreActif . "_id IN (" . implode(",", $userPerimetre[$perimetreActif]) . ")";
                    } else {
                        $sql .= " AND perimetre_" . $perimetreActif . "_id IS NULL";
                    }
                }
            }
        }

        try {
            $query = $this->getObjectManager()->getConnection()->prepare($sql);
        } catch (DBA_Exception $e) {
            throw new RuntimeException("Un problème est survenu lors de la récupération de la session.", 0, $e);
        }
        try {
            $res = $this->getObjectManager()->getConnection()->executeQuery($sql, [], []);
            $tmp = $res->fetchAllAssociative();
        } catch (DBA_Driver_Exception $e) {
            throw new RuntimeException("Un problème est survenu lors de la récupération de des données de l'indicateur.", 0, $e);
        }
        return $tmp;
    }


    public function manipulateDatabaseView(string $sql): void
    {
        try {
            $query = $this->getObjectManager()->getConnection()->prepare($sql);
        } catch (DBA_Exception $e) {
            throw new RuntimeException("Un problème est survenu lors de la récupération de la session.", 0, $e);
        }
        try {
            $query->execute();
        } catch (DBA_Driver_Exception $e) {
            throw new RuntimeException("Un problème est survenu lors de la récupération de des données de l'indicateur.", 0, $e);
        }
    }

    /**
     * @param Indicateur $indicateur
     */
    public function refresh(Indicateur $indicateur): void
    {
        $sql = "REFRESH MATERIALIZED VIEW " . $indicateur->getViewId();
        $this->manipulateDatabaseView($sql);
        $indicateur->setDernierRafraichissement(new DateTime());
        $count = $this->getCount($indicateur);
        $indicateur->setNbElements($count);
        $this->update($indicateur);
    }

    /**
     * @param Indicateur $indicateur
     */
    public function dropView(Indicateur $indicateur): void
    {
        $sql = "DROP MATERIALIZED VIEW IF EXISTS " . $indicateur->getViewId();
        $this->manipulateDatabaseView($sql);
    }

    /**
     * @param Indicateur $indicateur
     */
    public function createView(Indicateur $indicateur): void
    {
        $sql = "CREATE MATERIALIZED VIEW " . $indicateur->getViewId() . " AS " . $indicateur->getRequete();
        $this->manipulateDatabaseView($sql);
        $indicateur->setDernierRafraichissement(new DateTime());
        $count = $this->getCount($indicateur);
        $indicateur->setNbElements($count);
    }

    /**
     * @param Indicateur $indicateur
     */
    public function updateView(Indicateur $indicateur): void
    {
        $this->dropView($indicateur);
        $this->createView($indicateur);
        $count = $this->getCount($indicateur);
        $indicateur->setNbElements($count);
    }

    /** REQUETAGE *****************************************************************************************************/

    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(Indicateur::class)->createQueryBuilder('indicateur');
        return $qb;

    }

    /**
     * @param string $attribut
     * @param string $ordre
     * @return Indicateur[]
     */
    public function getIndicateurs(string $attribut = 'id', string $ordre = 'ASC'): array
    {
        $qb = $this->getObjectManager()->getRepository(Indicateur::class)->createQueryBuilder('indicateur')
            ->orderBy('indicateur.' . $attribut, $ordre);

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param int|null $id
     * @return Indicateur|null
     */
    public function getIndicateur(?int $id): ?Indicateur
    {
        if ($id === null) return null;

        $qb = $this->getObjectManager()->getRepository(Indicateur::class)->createQueryBuilder('indicateur')
            ->andWhere('indicateur.id = :id')
            ->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Indicateur partagent le même id [" . $id . "]", $e);
        }
        return $result;
    }

    /**
     * @param AbstractActionController $controller
     * @param string $paramName
     * @return Indicateur|null
     */
    public function getRequestedIndicateur(AbstractActionController $controller, string $paramName = "indicateur"): ?Indicateur
    {
        $id = $controller->params()->fromRoute($paramName);
        return $this->getIndicateur($id);
    }

    public function getIndicateurByCode(string $code): ?Indicateur
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('indicateur.code = :code')->setParameter('code', $code);
        try {
            $indicateur = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs [" . Indicateur::class . "] partagent le même code [" . $code . "]", 0, $e);
        }
        return $indicateur;
    }

    /**
     * @return Indicateur[]
     */
    public function getIndicateursByNamespace(?string $namespace): array
    {
        $qb = $this->getObjectManager()->getRepository(Indicateur::class)->createQueryBuilder('indicateur')
            ->andWhere('indicateur.namespace = :namespace')
            ->setParameter('namespace', $namespace);
        return $qb->getQuery()->getResult();
    }

    /** FACADE **************************************************************/

    /** RECUPERATION DONNEES ******************************************************************************************/

    /**
     * @param Indicateur $indicateur
     * @param ?string[] $perimetres (un tableau présentant les périmètres)
     * @return array|null
     */
    public function getIndicateurData(Indicateur $indicateur, ?array $perimetres = null): ?array
    {

        $exist = $this->verifierExistanceMaterializedView($indicateur->getViewId());
        if (!$exist) return null;

        $userPerimetre = $this->extractPerimetres($perimetres);
        $rawdata = $this->fetch($indicateur, $userPerimetre);
        if ($rawdata === null) return null;
        $rubriques = [];

        if ($indicateur->getEntity() === Indicateur::ENTITY_LIBRE) {
            if (!empty($rawdata)) {
                foreach ($rawdata[0] as $key => $value) {
                    $rubriques[] = $key;
                }
            }
        }
//        if ($indicateur->getEntity() === Indicateur::ENTITY_ADAPTATIF) {
//            if (!empty($rawdata)) {
//                foreach ($rawdata[0] as $key => $value) $rubriques[] = $key;
//            }
//        }
//        if ($indicateur->getEntity() === Indicateur::ENTITY_STRUCTURE) {
//            $rubriques = [
//                'Code' => 'code',
//                'Libelle' => 'libelle_court',
//                'Libelle long' => 'libelle_long',
//                'Type' => 'type',
//
//            ];
//        }
//        if ($indicateur->getEntity() === Indicateur::ENTITY_AGENT) {
//            $rubriques = [
//                'ID' => 'c_src_individu',
//                'SOURCE' => 'c_source',
//                'Prenom' => 'prenom',
//                'Nom' => 'nom_usage',
//            ];
//        }


        /** RETRAIT DES RUBRIQUES ASSOCIEES AUX PERIMETRES */
        $perimetresActifs = [];
        if ($indicateur->getPerimetre() !== null) { $perimetresActifs = explode(PerimetreServiceInterface::PERIMETRE_SEPARATOR, $indicateur->getPerimetre()); }
        foreach ($perimetresActifs as $perimetre) {
            $rubriques = array_diff($rubriques, ['perimetre_'.$perimetre.'_id']);
        }

        $data = [];
        foreach ($rawdata as $rawitem) {
            $item = [];
            foreach ($rubriques as $libelle => $code) {
                $item[] = $rawitem[$code];
            }
            $data[] = $item;
        }

        return [$rubriques, $data];
    }

    public function getCount(Indicateur $indicateur, ?array $perimetres = null): ?int
    {
        $exist = $this->verifierExistanceMaterializedView($indicateur->getViewId());
        if (!$exist) return null;

        $userPerimetre = $this->extractPerimetres($perimetres);
        $rawdata = $this->fetch($indicateur, $userPerimetre);
        return count($rawdata);
    }


    /**
     * @param string[]|null $perimetres
     * @return string[]
     */
    public function extractPerimetres(?array $perimetres = null): array
    {
        $array = [];

        if ($perimetres === null) return [];
        foreach ($perimetres as $perimetre) {
            $perimetre = strtolower($perimetre);
            [$type, $valeur] = explode('_', $perimetre);
            $array[$type][] = $valeur;
        }

        return $array;
    }
}