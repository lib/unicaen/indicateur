<?php

namespace UnicaenIndicateur\Service\Abonnement;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenIndicateur\Service\Indicateur\IndicateurService;
use Interop\Container\ContainerInterface;
use UnicaenMail\Service\Mail\MailService;

class AbonnementServiceFactory {

    /**
     * @param ContainerInterface $container
     * @return AbonnementService
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : AbonnementService
    {
        /**
         * @var EntityManager $entityManager
         * @var IndicateurService $indicateurService
         * @var MailService $mailService
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $indicateurService = $container->get(IndicateurService::class);
        $mailService = $container->get(MailService::class);

        $service = new AbonnementService();
        $service->setObjectManager($entityManager);
        $service->setIndicateurService($indicateurService);
        $service->setMailService($mailService);
        return $service;
    }
}