<?php

namespace UnicaenIndicateur\Service\Abonnement;

use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Laminas\Mvc\Controller\AbstractActionController;
use RuntimeException;
use UnicaenIndicateur\Entity\Db\Abonnement;
use UnicaenIndicateur\Entity\Db\Indicateur;
use UnicaenIndicateur\Service\Indicateur\IndicateurServiceAwareTrait;
use UnicaenMail\Service\Mail\MailServiceAwareTrait;
use UnicaenUtilisateur\Entity\Db\User;

/**
 * @property EntityManager $objectManager
 */
class AbonnementService {
    use IndicateurServiceAwareTrait;
    use MailServiceAwareTrait;
    use ProvidesObjectManager;

    /** GESTION DES ENTITES *******************************************************************************************/

    public function create(Abonnement $abonnement) : Abonnement
    {
        try {
            $this->getObjectManager()->persist($abonnement);
            $this->getObjectManager()->flush($abonnement);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en base.", $e);
        }
        return $abonnement;
    }

    public function update(Abonnement $abonnement) : Abonnement
    {
        try {
            $this->getObjectManager()->flush($abonnement);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en base.", $e);
        }
        return $abonnement;
    }

    public function delete(Abonnement $abonnement) : Abonnement
    {
        try {
            $this->getObjectManager()->remove($abonnement);
            $this->getObjectManager()->flush($abonnement);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en base.", $e);
        }
        return $abonnement;
    }

    /** REQUETAGE *****************************************************************************************************/

    public function createQueryBuilder() : QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(Abonnement::class)->createQueryBuilder('abonnement')
            ->join('abonnement.user', 'user')->addSelect('user')
            ->join('abonnement.indicateur', 'indicateur')->addSelect('indicateur')
        ;
        return $qb;
    }

    public function getAbonnements(string $attribut = 'id', string $ordre = 'ASC') : array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('abonnement.' . $attribut, $ordre)
        ;
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function getAbonnement(?int $id) : ?Abonnement
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('abonnement.id = :id')
            ->setParameter('id', $id)
            ;

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Abonnement partagent le même id [".$id."]",$e);
        }
        return $result;
    }

    public function getRequestedAbonnement(AbstractActionController $controller, string $paramName='abonnement') : ?Abonnement
    {
        $id = $controller->params()->fromRoute($paramName);
        return $this->getAbonnement($id);
    }

    /**
     * @param User $user
     * @return Abonnement[]
     */
    public function getAbonnementsByUser(User $user) : array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('abonnement.user = :user')
            ->setParameter('user', $user)
            ;
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param User $user
     * @param Indicateur $indicateur
     * @return Abonnement[]
     */
    public function getAbonnementsByUserAndIndicateur(User $user, Indicateur $indicateur) : array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('abonnement.user = :user')
            ->andWhere('abonnement.indicateur = :indicateur')
            ->setParameter('user', $user)
            ->setParameter('indicateur', $indicateur)
        ;

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /** FONCTIONNEMENT ************************************************************************************************/

    public function notifyAbonnements() : void
    {
        $indicateurs = $this->getIndicateurService()->getIndicateurs();

        foreach ($indicateurs as $indicateur) {
            $abonnements = $indicateur->getAbonnements();
            if (!empty($abonnements)) {

                $titre = "Publication de l'indicateur [".$indicateur->getTitre()."] (". (new DateTime())->format("d/m/Y à H:i:s").")";
                $result = $this->getIndicateurService()->getIndicateurData($indicateur);
                $texte  = "<table>";
                $texte .= "<thead>";
                $texte .= "<tr>";
                foreach ($result[0] as $rubrique) $texte .= "<th>" . $rubrique . "</th>";
                $texte .= "</tr>";
                $texte .= "</thead>";
                $texte .= "<tbody>";
                foreach ($result[1] as $item) {
                    $texte .="<tr>";
                    foreach ($item as $value) $texte .="<td>". $value ."</td>";
                    $texte .="</tr>";
                }
                $texte .= "</tbody>";
                $texte .= "</table>";

                foreach ($abonnements as $abonnement) {
                    $adresse = $abonnement->getUser()->getEmail();
                    $mail = $this->getMailService()->sendMail($adresse, $titre, $texte);
                    $mail->setMotsClefs([$indicateur->generateTag()]);
                    $this->getMailService()->update($mail);
                    $abonnement->setDernierEnvoi(new DateTime());
                    $this->update($abonnement);
                }
            }
        }
    }

    public function isAbonner(?User $user, ?Indicateur $indicateur) : bool
    {
        foreach ($indicateur->getAbonnements() as $abonnement) {
            if ($abonnement->getUser() === $user) return true;
        }
        return false;
    }

}