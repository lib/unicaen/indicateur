<?php

namespace UnicaenIndicateur\Service\TableauDeBord;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Laminas\Mvc\Controller\AbstractActionController;
use RuntimeException;
use UnicaenIndicateur\Entity\Db\TableauDeBord;

/**
 * @property EntityManager $objectManager
 */
class TableauDeBordService {
    use ProvidesObjectManager;

    /** GESTION DES ENTITES *****************************************/

    public function create(TableauDeBord $tableau) : TableauDeBord
    {
        $this->getObjectManager()->persist($tableau);
        $this->getObjectManager()->flush($tableau);
        return $tableau;
    }

    public function update(TableauDeBord $tableau) : TableauDeBord
    {
        $this->getObjectManager()->flush($tableau);
        return $tableau;
    }

    public function delete(TableauDeBord $tableau) : TableauDeBord
    {
        $this->getObjectManager()->remove($tableau);
        $this->getObjectManager()->flush($tableau);
        return $tableau;
    }

    /** REQUETAGE ***************************************************/

    public function createQueryBuilder() : QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(TableauDeBord::class)->createQueryBuilder('tableau')
            ->leftJoin('tableau.indicateurs', 'indicateur')->addSelect('indicateur');
        return $qb;
    }

    /**
     * @param string $champ
     * @param string $ordre
     * @return TableauDeBord[]
     */
    public function getTableauxdeBord(string $champ = 'id', string $ordre='ASC') : array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('tableau.'.$champ, $ordre);
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function getTableauDeBord(?int $id) : ?TableauDeBord
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('tableau.id = :id')->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs TableauDeBord partagent le même id [".$id."]");
        }
        return $result;
    }

    public function getRequestedTableauDeBord(AbstractActionController $controller, string $param='tableau-de-bord') : ?TableauDeBord
    {
        $id = $controller->params()->fromRoute($param);
        $result = $this->getTableauDeBord($id);
        return $result;
    }

    /**
     * @return TableauDeBord[]
     */
    public function getTableauxDeBordByNamespace(?string $namespace) : array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('tableau.namespace = :namespace')
            ->setParameter('namespace', $namespace);
        return $qb->getQuery()->getResult();
    }

    /** FACADE ******************************************************/
}