<?php

namespace UnicaenIndicateur\Service\TableauDeBord;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;

class TableauDeBordServiceFactory {

    public function __invoke(ContainerInterface $container) : TableauDeBordService
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        $service = new TableauDeBordService();
        $service->setObjectManager($entityManager);
        return $service;
    }
}