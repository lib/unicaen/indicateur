<?php

namespace UnicaenIndicateur\Service\TableauDeBord;

trait TableauDeBordServiceAwareTrait {

    private TableauDeBordService $tableauDeBordService;

    public function getTableauDeBordService(): TableauDeBordService
    {
        return $this->tableauDeBordService;
    }

    public function setTableauDeBordService(TableauDeBordService $tableauDeBordService): void
    {
        $this->tableauDeBordService = $tableauDeBordService;
    }

}