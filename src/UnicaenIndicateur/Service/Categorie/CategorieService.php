<?php

namespace UnicaenIndicateur\Service\Categorie;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Laminas\Mvc\Controller\AbstractActionController;
use RuntimeException;
use UnicaenIndicateur\Entity\Db\Categorie;

/**
 * @property EntityManager $objectManager
 */
class CategorieService
{
    use ProvidesObjectManager;

    /** GESTION DES ENTITES ***********************************************************************************************/

    public function create(Categorie $categorie): Categorie
    {
        $this->getObjectManager()->persist($categorie);
        $this->getObjectManager()->flush($categorie);
        return $categorie;
    }

    public function update(Categorie $categorie): Categorie
    {
        $this->getObjectManager()->flush($categorie);
        return $categorie;
    }

    public function delete(Categorie $categorie): Categorie
    {
        $this->getObjectManager()->remove($categorie);
        $this->getObjectManager()->flush($categorie);
        return $categorie;
    }

    /** REQUETAGE *****************************************************************************************************/

    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(Categorie::class)->createQueryBuilder('categorie')
            ->leftJoin('categorie.indicateurs', 'indicateur')->addSelect('indicateur');
        return $qb;
    }

    /** @return Categorie[] */
    public function getCategories(string $champ = 'ordre', string $ordre = 'ASC'): array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('categorie.' . $champ, $ordre);

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function getCategoriesAsOptions(string $champ = 'ordre', string $ordre = 'ASC'): array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('categorie.' . $champ, $ordre);

        $result = $qb->getQuery()->getResult();

        $options = [];
        foreach ($result as $item) {
            $options[$item->getId()] = $item->getLibelle();
        }
        return $options;
    }

    public function getCategorie(?int $id): ?Categorie
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('categorie.id = :id')->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs [" . Categorie::class . "] partagent le même id [" . $id . "]", $e);
        }
        return $result;
    }

    /**
     * @param AbstractActionController $controller
     * @param string $paramName
     * @return Categorie|null
     */
    public function getRequestedCategorie(AbstractActionController $controller, string $paramName = "categorie"): ?Categorie
    {
        $id = $controller->params()->fromRoute($paramName);
        return $this->getCategorie($id);
    }

    public function getCategorieByCode(?string $code): ?Categorie
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('categorie.code = :code')->setParameter('code', $code);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs [" . Categorie::class . "] partagent le même code [" . $code . "]", $e);
        }
        return $result;
    }

    /** FACADE **************************************************************/


}