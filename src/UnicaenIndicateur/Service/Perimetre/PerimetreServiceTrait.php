<?php

namespace UnicaenIndicateur\Service\Perimetre;

trait PerimetreServiceTrait {

    private array $perimetres = [];

    public function getListePerimetres(): array
    {
        return $this->perimetres;
    }

    public function setListePerimetres(array $liste): void
    {
        $this->perimetres = $liste;
    }
}