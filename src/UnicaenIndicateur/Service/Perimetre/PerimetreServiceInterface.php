<?php

namespace UnicaenIndicateur\Service\Perimetre;

use UnicaenUtilisateur\Entity\Db\AbstractRole;
use UnicaenUtilisateur\Entity\Db\AbstractUser;

interface PerimetreServiceInterface {

    const PERIMETRE_SEPARATOR = '||';

    public function setListePerimetres(array $liste): void;
    public function getListePerimetres(): array;
    public function getPerimetres(AbstractUser $user, AbstractRole $role) : array;
}