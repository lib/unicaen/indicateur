<?php

namespace UnicaenIndicateur\Service\Perimetre;

trait PerimetreServiceAwareTrait
{
    private  PerimetreServiceInterface $perimetreService;

    public function getPerimetreService(): PerimetreServiceInterface
    {
        return $this->perimetreService;
    }

    public function setPerimetreService(PerimetreServiceInterface $perimetreService): void
    {
        $this->perimetreService = $perimetreService;
    }

}