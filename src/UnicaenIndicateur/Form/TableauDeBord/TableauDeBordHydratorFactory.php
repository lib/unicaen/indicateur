<?php

namespace UnicaenIndicateur\Form\TableauDeBord;

use Psr\Container\ContainerInterface;

class TableauDeBordHydratorFactory {

    public function __invoke(ContainerInterface $container) : TableauDeBordHydrator
    {
        $hydrator = new TableauDeBordHydrator();
        return $hydrator;
    }
}