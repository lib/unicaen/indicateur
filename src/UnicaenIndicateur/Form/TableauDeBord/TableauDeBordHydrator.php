<?php

namespace UnicaenIndicateur\Form\TableauDeBord;

use Laminas\Hydrator\HydratorInterface;
use UnicaenIndicateur\Entity\Db\TableauDeBord;

class TableauDeBordHydrator implements HydratorInterface {

    /**
     * @param TableauDeBord $object
     * @return array
     */
    public function extract(object $object): array
    {
        $data = [
            'libelle' => $object->getTitre(),
            'description' => $object->getDescription(),
            'namespace' => $object->getNamespace(),
            'nb_colonne' => $object->getNbColumn(),
        ];
        return $data;
    }

    /**
     * @param array $data
     * @param TableauDeBord $object
     * @return TableauDeBord
     */
    public function hydrate(array $data, object $object) : object
    {
        $libelle = (isset($data['libelle']) AND trim($data['libelle']) !== '')?trim($data['libelle']):null;
        $description = (isset($data['description']) AND trim($data['description']) !== '')?trim($data['description']):null;
        $nbColonne = (isset($data['nb_colonne']))?$data['nb_colonne']:null;
        $namespace = (isset($data['namespace']) AND trim($data['namespace']) !== '')?trim($data['namespace']):null;

        $object->setTitre($libelle);
        $object->setDescription($description);
        $object->setNbColumn($nbColonne);
        $object->setNamespace($namespace);

        return $object;
    }


}