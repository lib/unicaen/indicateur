<?php

namespace UnicaenIndicateur\Form\TableauDeBord;

use Laminas\Form\Element\Button;
use Laminas\Form\Element\Number;
use Laminas\Form\Element\Text;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;

class TableauDeBordForm extends Form {

    public function init()
    {
        // libelle
        $this->add([
            'type' => Text::class,
            'name' => 'libelle',
            'options' => [
                'label' => "Libelle* :",
            ],
            'attributes' => [
                'id' => 'libelle',
            ],
        ]);
        // description
        $this->add([
            'name' => 'description',
            'type' => 'textarea',
            'options' => [
                'label' => 'Description : ',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'class' => 'type2 form-control',
            ]
        ]);
        // libelle
        $this->add([
            'type' => Text::class,
            'name' => 'namespace',
            'options' => [
                'label' => "Namespace :",
            ],
            'attributes' => [
                'id' => 'namespace',
            ],
        ]);
        // nbCOlum
        $this->add([
            'name' => 'nb_colonne',
            'type' => Number::class,
            'options' => [
                'label' => 'Nombre de colonne : ',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'id' => 'nb_colonne',
                'min' => 1,
                'max' => 3,
            ],
        ]);

        // submit
        $this->add([
            'type' => Button::class,
            'name' => 'creer',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer',
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
            ],
        ]);


        $this->setInputFilter((new Factory())->createInputFilter([
            'libelle'               => [ 'required' => true,  ],
            'description'           => [ 'required' => false,  ],
            'namespace'             => [ 'required' => false,  ],
            'nb_colonne'            => [ 'required' => true,  ],
        ]));
    }
}