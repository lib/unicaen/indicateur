<?php

namespace UnicaenIndicateur\Form\TableauDeBord;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class TableauDeBordFormFactory {

    /**
     * @param ContainerInterface $container
     * @return TableauDeBordForm
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : TableauDeBordForm
    {
        /**
         * @var TableauDeBordHydrator $hydrator
         */
        $hydrator = $container->get('HydratorManager')->get(TableauDeBordHydrator::class);

        $form = new TableauDeBordForm();
        $form->setHydrator($hydrator);
        return $form;
    }
}