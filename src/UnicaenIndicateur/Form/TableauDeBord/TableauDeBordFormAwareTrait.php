<?php

namespace UnicaenIndicateur\Form\TableauDeBord;

trait TableauDeBordFormAwareTrait {

    private TableauDeBordForm $tableauDeBordForm;

    public function getTableauDeBordForm(): TableauDeBordForm
    {
        return $this->tableauDeBordForm;
    }

    public function setTableauDeBordForm(TableauDeBordForm $tableauDeBordForm): void
    {
        $this->tableauDeBordForm = $tableauDeBordForm;
    }
}