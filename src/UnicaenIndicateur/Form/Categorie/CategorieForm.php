<?php

namespace UnicaenIndicateur\Form\Categorie;

use Laminas\Form\Element\Button;
use Laminas\Form\Element\Number;
use Laminas\Form\Element\Text;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;
use Laminas\Validator\Callback;
use UnicaenIndicateur\Service\Categorie\CategorieServiceAwareTrait;

class CategorieForm extends Form
{
    use CategorieServiceAwareTrait;

    private ?string $oldCode = null;

    public function setOldCode(?string $oldCode): void
    {
        $this->oldCode = $oldCode;
    }

    public function init(): void
    {
        // code
        $this->add([
            'type' => Text::class,
            'name' => 'code',
            'options' => [
                'label' => "Code unique identifiant la catégorie <span class='icon icon-obligatoire' title='champ obligatoire'></span> :",
                'label_options' => ['disable_html_escape' => true,],
            ],
            'attributes' => [
                'id' => 'code',
            ],
        ]);
        // libelle
        $this->add([
            'type' => Text::class,
            'name' => 'libelle',
            'options' => [
                'label' => "Libellé <span class='icon icon-obligatoire' title='champ obligatoire'></span> :",
                'label_options' => ['disable_html_escape' => true,],
            ],
            'attributes' => [
                'id' => 'libelle',
            ],
        ]);
        // description
        $this->add([
            'name' => 'description',
            'type' => 'textarea',
            'options' => [
                'label' => 'Description : ',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'class' => 'type2 form-control',
            ]
        ]);
        // ordre
        $this->add([
            'type' => Number::class,
            'name' => 'ordre',
            'options' => [
                'label' => "Ordre <span class='icon icon-obligatoire' title='champ obligatoire'></span> :",
                'label_options' => ['disable_html_escape' => true,],
            ],
            'attributes' => [
                'id' => 'ordre',
                'min' => 0,
                'max' => 9999,
            ],
        ]);
        // submit
        $this->add([
            'type' => Button::class,
            'name' => 'creer',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer',
                'label_options' => ['disable_html_escape' => true,],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
            ],
        ]);


        $this->setInputFilter((new Factory())->createInputFilter([
            'code' => [
                'required' => true,
                'validators' => [[
                    'name' => Callback::class,
                    'options' => [
                        'messages' => [
                            Callback::INVALID_VALUE => "Ce code existe déjà",
                        ],
                        'callback' => function ($value, $context = []) {
                            if ($value == $this->oldCode) return true;
//                            return ($this->getEntityManager()->getRepository(EtatType::class)->findOneBy(['code'=>$value],[]) == null);;
                            return ($this->getCategorieService()->getCategorieByCode($value) == null);
                        },
                        //'break_chain_on_failure' => true,
                    ],
                ]],
            ],

            'libelle' => ['required' => true,],
            'description' => ['required' => false,],
            'ordre' => ['required' => true,],
        ]));
    }
}