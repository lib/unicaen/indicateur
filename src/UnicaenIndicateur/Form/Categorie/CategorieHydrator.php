<?php

namespace UnicaenIndicateur\Form\Categorie;

use UnicaenIndicateur\Entity\Db\Categorie;
use Laminas\Hydrator\HydratorInterface;

class CategorieHydrator implements HydratorInterface {

    public function extract($object): array
    {
        /** @var Categorie $object */
        $data = [
            'code' => $object->getCode(),
            'libelle' => $object->getLibelle(),
            'description' => $object->getDescription(),
            'ordre' => $object->getOrdre(),
        ];
        return $data;
    }

    public function hydrate(array $data, object $object) : object
    {
        $code = (isset($data['code']) AND trim($data['code']) !== '')?trim($data['code']):null;
        $libelle = (isset($data['libelle']) AND trim($data['libelle']) !== '')?trim($data['libelle']):null;
        $description = (isset($data['description']) AND trim($data['description']) !== '')?trim($data['description']):null;
        $ordre = (isset($data['ordre']) AND trim($data['ordre']) !== '')?trim($data['ordre']):null;

        /** @var Categorie $object */
        $object->setCode($code);
        $object->setLibelle($libelle);
        $object->setDescription($description);
        $object->setOrdre($ordre);
        return $object;
    }

}