<?php

namespace UnicaenIndicateur\Form\Indicateur;

use Laminas\Validator\Callback;
use UnicaenIndicateur\Entity\Db\Indicateur;
use Laminas\Form\Element\Button;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Text;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;
use UnicaenIndicateur\Service\Categorie\CategorieServiceAwareTrait;
use UnicaenIndicateur\Service\Indicateur\IndicateurServiceAwareTrait;

class IndicateurForm extends Form {
    use IndicateurServiceAwareTrait;
    use CategorieServiceAwareTrait;

    private ?string $oldCode = null;
    private array $perimetres = [];

    public function setOldCode(?string $oldCode): void
    {
        $this->oldCode = $oldCode;
    }

    public function setPerimetres(array $perimetres): void
    {
        $this->perimetres = $perimetres;
    }

    public function init(): void
    {
        // code
        $this->add([
            'type' => Text::class,
            'name' => 'code',
            'options' => [
                'label' => "Code unique identifiant l'indicateur <span class='icon icon-obligatoire' title='champ obligatoire'></span> :",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'code',
            ],
        ]);
        // libelle
        $this->add([
            'type' => Text::class,
            'name' => 'libelle',
            'options' => [
                'label' => "Libelle<span class='icon icon-obligatoire' title='champ obligatoire'></span>  :",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'libelle',
            ],
        ]);
        // description
        $this->add([
            'name' => 'description',
            'type' => 'textarea',
            'options' => [
                'label' => 'Description : ',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'class' => 'type2 form-control',
            ]
        ]);
        // categorie
        $this->add([
            'type' => Select::class,
            'name' => 'categorie',
            'options' => [
                'label' => "Catégorie :",
                'empty_option' => "Sélectionner une entité ...",
                'value_options' => $this->getCategorieService()->getCategoriesAsOptions(),
            ],
            'attributes' => [
                'id' => 'categorie',
            ],
        ]);
        // namespace
        $this->add([
            'type' => Text::class,
            'name' => 'namespace',
            'options' => [
                'label' => "Namespace :",
            ],
            'attributes' => [
                'id' => 'namespace',
            ],
        ]);
        // view_id
        $this->add([
            'type' => Text::class,
            'name' => 'view_id',
            'options' => [
                'label' => "Identifiant de la vue <span class='icon icon-obligatoire' title='champ obligatoire'></span>  :",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'libelle',
            ],
        ]);
        // entity
        $this->add([
            'type' => Select::class,
            'name' => 'entity',
            'options' => [
                'label' => "Entity associé <span class='icon icon-obligatoire' title='champ obligatoire'></span>  :",
                'label_options' => [ 'disable_html_escape' => true, ],
                'empty_option' => "Sélectionner une entité ...",
                'value_options' => [
                   Indicateur::ENTITY_LIBRE       => 'Libre',
                   Indicateur::ENTITY_ADAPTATIF   => 'Adaptatif',
                   Indicateur::ENTITY_AGENT       => 'Agent',
                   Indicateur::ENTITY_STRUCTURE   => 'Structure',
                ]
            ],
            'attributes' => [
                'id' => 'formations',
                'class'             => 'bootstrap-selectpicker show-tick',
            ],
        ]);
        // perimetre
        $this->add([
            'type' => Select::class,
            'name' => 'perimetre',
            'options' => [
                'label' => "Périmètres associés  <span class='icon icon-information text-info' title='Sélection multiple possible'></span>  :",
                'label_options' => [ 'disable_html_escape' => true, ],
                'empty_option' => "Sélection des périmètres",
                'value_options' => $this->perimetres,
            ],
            'attributes' => [
                'id' => 'perimetre',
                'class' => 'selectpicker show-tick',
                'multiple' => 'multiple',
            ],
        ]);
        // requete
        $this->add([
            'name' => 'requete',
            'type' => 'textarea',
            'options' => [
                'label' => "Requête <span class='icon icon-obligatoire' title='Champ obligatoire'></span> : ",
                'label_options' => [ 'disable_html_escape' => true, ],
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'id' => 'requete',
                'class' => 'form-control',
                'style' => 'min-height:250px;',
            ]
        ]);
        // submit
        $this->add([
            'type' => Button::class,
            'name' => 'creer',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer',
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
            ],
        ]);


        $this->setInputFilter((new Factory())->createInputFilter([
            'code'                  => [
                'required' => true,
                'validators' => [[
                    'name' => Callback::class,
                    'options' => [
                        'messages' => [
                            Callback::INVALID_VALUE => "Ce code existe déjà",
                        ],
                        'callback' => function ($value, $context = []) {
                            if($value == $this->oldCode) return true;
//                            return ($this->getEntityManager()->getRepository(EtatType::class)->findOneBy(['code'=>$value],[]) == null);;
                            return ($this->getIndicateurService()->getIndicateurByCode($value) == null);
                        },
                        //'break_chain_on_failure' => true,
                    ],
                ]],
            ],

            'libelle'               => [ 'required' => true,  ],
            'description'           => [ 'required' => false,  ],
            'categorie'             => [ 'required' => false,  ],
            'namespace'             => [ 'required' => false,  ],
            'view_id'               => [ 'required' => true,  ],
            'entity'                => [ 'required' => true,  ],
            'perimetre'             => [ 'required' => false,  ],
            'requete'               => [ 'required' => true,  ],
        ]));
    }
}