<?php

namespace UnicaenIndicateur\Form\Indicateur;

trait IndicateurFormAwareTrait
{

    private IndicateurForm $indicateurForm;

    public function getIndicateurForm() : IndicateurForm
    {
        return $this->indicateurForm;
    }

    public function setIndicateurForm(IndicateurForm $indicateurForm) : void
    {
        $this->indicateurForm = $indicateurForm;
    }

}