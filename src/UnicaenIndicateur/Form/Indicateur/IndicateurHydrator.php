<?php

namespace UnicaenIndicateur\Form\Indicateur;

use UnicaenIndicateur\Entity\Db\Indicateur;
use Laminas\Hydrator\HydratorInterface;
use UnicaenIndicateur\Service\Categorie\CategorieServiceAwareTrait;
use UnicaenIndicateur\Service\Perimetre\PerimetreServiceInterface;

class IndicateurHydrator implements HydratorInterface {
    use CategorieServiceAwareTrait;

    public function extract($object): array
    {
        /** @var Indicateur $object */
        $data = [
            'code' => $object->getCode(),
            'libelle' => $object->getTitre(),
            'description' => $object->getDescription(),
            'categorie' => $object->getCategorie()?->getId(),
            'namespace' => $object->getNamespace(),
            'view_id' => $object->getViewId(),
            'entity' => $object->getEntity(),
            'perimetre' => ($object->getPerimetre())?explode(PerimetreServiceInterface::PERIMETRE_SEPARATOR, $object->getPerimetre()):null,
            'requete' => $object->getRequete(),
        ];
        return $data;
    }

    public function hydrate(array $data, object $object) : object
    {
        $code = (isset($data['code']) AND trim($data['code']) !== '')?trim($data['code']):null;
        $categorie = (isset($data['categorie']) && $data['categorie'] !== '')?$this->getCategorieService()->getCategorie($data['categorie']):null;
        $namespace = (isset($data['namespace']) AND trim($data['namespace']) !== '')?trim($data['namespace']):null;

        $perimetres = isset($data['perimetre'])?implode(PerimetreServiceInterface::PERIMETRE_SEPARATOR, $data['perimetre']):null;
        //HERE I AM// $perimetreIds = (isset($data['perimetre']) AND ($data['perimetre']) !== '')?explode(PerimetreServiceInterface::PERIMETRE_SEPARATOR, $data['perimetre']):null;

        /** @var Indicateur $object */
        $object->setCode($code);
        $object->setTitre($data['libelle']);
        $object->setDescription($data['description']);
        $object->setCategorie($categorie);
        $object->setNamespace($namespace);
        $object->setPerimetre($perimetres);
        $object->setViewId($data['view_id']);
        $object->setEntity($data['entity']);
        $object->setRequete($data['requete']);
        return $object;
    }

}