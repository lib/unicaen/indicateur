<?php

namespace UnicaenIndicateur\Form\Indicateur;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenIndicateur\Service\Categorie\CategorieService;
use UnicaenIndicateur\Service\Indicateur\IndicateurService;
use UnicaenIndicateur\Service\Perimetre\PerimetreServiceInterface;

class IndicateurFormFactory {

    /**
     * @param ContainerInterface $container
     * @return IndicateurForm
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : IndicateurForm
    {
        /**
         * @var CategorieService $categorieService
         * @var IndicateurService $indicateurService
         * @var IndicateurHydrator $hydrator */
        $categorieService = $container->get(CategorieService::class);
        $indicateurService = $container->get(IndicateurService::class);
        $hydrator = $container->get('HydratorManager')->get(IndicateurHydrator::class);

        $config = $container->get('config');
        /** @var PerimetreServiceInterface $perimetreService */
        $perimetreServiceKey = $container->get($config['unicaen-indicateur']['perimetreService']);
        $perimetreService = $container->get($perimetreServiceKey::class);
        /** @var array $perimetres */
        $perimetres = $config['unicaen-indicateur']['perimetres'];

        $form = new IndicateurForm();
        $form->setCategorieService($categorieService);
        $form->setIndicateurService($indicateurService);
        $form->setHydrator($hydrator);
        $form->setPerimetres($perimetres);
        return $form;
    }
}