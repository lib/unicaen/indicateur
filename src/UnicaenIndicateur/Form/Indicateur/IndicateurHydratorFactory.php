<?php

namespace UnicaenIndicateur\Form\Indicateur;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenIndicateur\Service\Categorie\CategorieService;

class IndicateurHydratorFactory {

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : IndicateurHydrator
    {
        /** @var CategorieService $categorieService */
        $categorieService = $container->get(CategorieService::class);

        $hydrator = new IndicateurHydrator();
        $hydrator->setCategorieService($categorieService);
        return $hydrator;
    }
}