<?php

namespace UnicaenIndicateur\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class IndicateurPrivileges extends Privileges
{
    const INDICATEUR_MES_INDICATEURS = 'indicateur-indicateur_mes_indicateurs';
    const INDICATEUR_INDEX = 'indicateur-indicateur_index';
    const AFFICHER_INDICATEUR = 'indicateur-afficher_indicateur';
    const EDITER_INDICATEUR = 'indicateur-editer_indicateur';
    const DETRUIRE_INDICATEUR = 'indicateur-detruire_indicateur';
}