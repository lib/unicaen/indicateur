<?php

namespace UnicaenIndicateur\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class TableaudebordPrivileges extends Privileges
{
    const AFFICHER_TABLEAUDEBORD = 'tableaudebord-afficher_tableaudebord';
    const EDITER_TABLEAUDEBORD = 'tableaudebord-editer_tableaudebord';
    const DETRUIRE_TABLEAUDEBORD = 'tableaudebord-detruire_tableaudebord';
}