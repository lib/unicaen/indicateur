<?php

namespace UnicaenIndicateur\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class AbonnementPrivileges extends Privileges
{
    const AFFICHER_ABONNEMENT = 'abonnement-afficher_abonnement';
    const EDITER_ABONNEMENT = 'abonnement-editer_abonnement';
    const DETRUIRE_ABONNEMENT = 'abonnement-detruire_abonnement';
}