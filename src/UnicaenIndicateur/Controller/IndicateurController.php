<?php

namespace UnicaenIndicateur\Controller;

use DateTime;
use Laminas\Http\Response;
use UnicaenIndicateur\Entity\Db\Indicateur;
use UnicaenIndicateur\Entity\Db\Categorie;
use UnicaenIndicateur\Form\Indicateur\IndicateurFormAwareTrait;
use UnicaenIndicateur\Service\Abonnement\AbonnementServiceAwareTrait;
use UnicaenIndicateur\Service\Indicateur\IndicateurServiceAwareTrait;
use UnicaenApp\View\Model\CsvModel;
use UnicaenIndicateur\Service\Perimetre\PerimetreServiceAwareTrait;
use UnicaenMail\Service\Mail\MailServiceAwareTrait;
use UnicaenUtilisateur\Service\User\UserServiceAwareTrait;
use Laminas\Http\Request;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class IndicateurController extends AbstractActionController {
    use IndicateurServiceAwareTrait;
    use UserServiceAwareTrait;
    use AbonnementServiceAwareTrait;
    use MailServiceAwareTrait;
    use PerimetreServiceAwareTrait;

    use IndicateurFormAwareTrait;

    public function indexAction() : ViewModel
    {
        $indicateurs = $this->getIndicateurService()->getIndicateurs();
        $user = $this->getUserService()->getConnectedUser();
        $abonnements = $this->getAbonnementService()->getAbonnementsByUser($user);

        $sans = new Categorie();
        $sans->setId(-1); $sans->setLibelle("Sans catégorie"); $sans->setOrdre(99999);
        $categories = []; $indicateursByCategorie = [];
        foreach ($indicateurs as $indicateur) {
            if ($indicateur->getCategorie()) {
                $categorie = $indicateur->getCategorie();
                $categories[$categorie->getId()] = $categorie;
                $indicateursByCategorie[$categorie->getId()][] = $indicateur;
            } else {
                $categories[-1] = $sans;
                $indicateursByCategorie[-1][] = $indicateur;
            }
        }

        $array = [];
        foreach ($abonnements as $abonnement) {
            $array [$abonnement->getIndicateur()->getId()] = $abonnement;
        }

        usort($categories, function(Categorie $a, Categorie $b) { return $a->getOrdre() <=> $b->getOrdre(); });
        return new ViewModel([
            'categories' => $categories,
            'indicateursByCategorie' => $indicateursByCategorie,
            'indicateurs' => $indicateurs,
            'abonnements' => $array,
        ]);
    }

    public function afficherAction() : ViewModel
    {
        $indicateur = $this->getIndicateurService()->getRequestedIndicateur($this);
        $mails = $this->getMailService()->getMailsByMotClef($indicateur->generateTag());

        $user = $this->getUserService()->getConnectedUser();
        $role = $this->getUserService()->getConnectedRole();
        $perimetres = $this->getPerimetreService()->getPerimetres($user, $role);


        $exists = $this->getIndicateurService()->verifierExistanceMaterializedView($indicateur->getViewId());
        if ($exists === true) $result = $this->getIndicateurService()->getIndicateurData($indicateur, $perimetres);

        return new ViewModel([
            'indicateur' => $indicateur,
            'exists' => $exists,
            'header' => ($exists)?$result[0]:null,
            'data' =>   ($exists)?$result[1]:null,
            'mails' => $mails,
        ]);
    }

    public function rafraichirAction() : Response
    {
        $indicateur = $this->getIndicateurService()->getRequestedIndicateur($this);
        $this->getIndicateurService()->refresh($indicateur);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('indicateur/afficher', ['indicateur' => $indicateur->getId()], [], true);
    }

    public function creerAction() : ViewModel|Response
    {
        $indicateur = new Indicateur();
        $namespace = $this->params()->fromQuery('namespace');

        $form = $this->getIndicateurForm();
        $form->setAttribute('action', $this->url()->fromRoute('indicateur/creer', [], ['query' => ['namespace' => $namespace]], true));
        if ($namespace) {
            $indicateur->setNamespace($namespace);
            $form->get('namespace')->setAttribute('readonly', true);
        }
        $form->bind($indicateur);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $indicateur->setDernierRafraichissement(new DateTime());

                $this->getIndicateurService()->create($indicateur);
                $this->getIndicateurService()->createView($indicateur);
                $count = $this->getIndicateurService()->getCount($indicateur);
                $indicateur->setNbElements($count);
                $this->getIndicateurService()->update($indicateur);
                return $this->redirect()->toRoute('indicateur/afficher', ['indicateur' => $indicateur->getId()], [], true);
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('unicaen-indicateur/default/default-form');
        $vm->setVariables([
            'title' => 'Création d\'un nouvel indicateur',
            'form'  => $form,
        ]);
        return $vm;
    }

    public function modifierAction(): ViewModel|Response
    {
        $indicateur = $this->getIndicateurService()->getRequestedIndicateur($this);
        $namespace = $this->params()->fromQuery('namespace');

        $form = $this->getIndicateurForm();
        $form->setOldCode($indicateur->getCode());
        $form->setAttribute('action', $this->url()->fromRoute('indicateur/modifier', ['indicateur' => $indicateur->getId()], ['query' => ['namespace' => $namespace]], true));
        if ($namespace) {
            $indicateur->setNamespace($namespace);
            $form->get('namespace')->setAttribute('readonly', true);
        }
        $form->bind($indicateur);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getIndicateurService()->update($indicateur);
                $this->getIndicateurService()->updateView($indicateur);
                return $this->redirect()->toRoute('indicateur/afficher', ['indicateur' => $indicateur->getId()], [], true);
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('unicaen-indicateur/default/default-form');
        $vm->setVariables([
            'title' => 'Modification d\'un indicateur',
            'form'  => $form,
        ]);
        return $vm;
    }

    public function detruireAction(): ViewModel
    {
        $indicateur = $this->getIndicateurService()->getRequestedIndicateur($this);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") {
                $this->getIndicateurService()->dropView($indicateur);
                $this->getIndicateurService()->delete($indicateur);
            }
            exit();
        }

        $vm = new ViewModel();
        if ($indicateur !== null) {
            $vm->setTemplate('unicaen-indicateur/default/confirmation');
            $vm->setVariables([
                'title' => "Suppression de l'indicateur " . $indicateur->getTitre(),
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('indicateur/detruire', ["indicateur" => $indicateur->getId()], [], true),
            ]);
        }
        return $vm;
    }

    public function regenererAction() : Response
    {
        $indicateur = $this->getIndicateurService()->getRequestedIndicateur($this);
        $indicateur->setDernierRafraichissement(new DateTime());
        $this->getIndicateurService()->createView($indicateur);
        $this->getIndicateurService()->update($indicateur);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('indicateur/afficher', ['indicateur' => $indicateur->getId()], [], true);
    }

    public function exporterAction(): CsvModel
    {
        $indicateur = $this->getIndicateurService()->getRequestedIndicateur($this);
        $date = new DateTime();
        $filename = "preecog_".$indicateur->getViewId()."_".$date->format('Ymd-Hms').".csv";

        $user = $this->getUserService()->getConnectedUser();
        $role = $this->getUserService()->getConnectedRole();
        $perimetres = $this->getPerimetreService()->getPerimetres($user, $role);

        $result = $this->getIndicateurService()->getIndicateurData($indicateur, $perimetres);

        $CSV = new CsvModel();
        $CSV->setDelimiter(';');
        $CSV->setEnclosure('"');
        $CSV->setHeader($result[0]);
        $CSV->setData($result[1]);
        $CSV->setFilename($filename);

        return $CSV;
    }

    public function rafraichirTousAction() : void
    {
        $indicateurs = $this->getIndicateurService()->getIndicateurs();
        foreach ($indicateurs as $indicateur) {
            $this->getIndicateurService()->refresh($indicateur);
        }
    }

}