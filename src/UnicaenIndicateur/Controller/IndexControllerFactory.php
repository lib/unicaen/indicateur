<?php

namespace UnicaenIndicateur\Controller;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenIndicateur\Service\Abonnement\AbonnementService;
use UnicaenIndicateur\Service\Indicateur\IndicateurService;
use UnicaenUtilisateur\Service\User\UserService;

class IndexControllerFactory {

    /**
     * @param ContainerInterface $container
     * @return IndexController
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : IndexController
    {
        /**
         * @var AbonnementService $abonnementService
         * @var IndicateurService $indicateurService
         * @var UserService $userService
         */
        $abonnementService = $container->get(AbonnementService::class);
        $indicateurService = $container->get(IndicateurService::class);
        $userService = $container->get(UserService::class);

        $controller  = new IndexController();
        $controller->setAbonnementService($abonnementService);
        $controller->setIndicateurService($indicateurService);
        $controller->setUserService($userService);
        return $controller;
    }
}