<?php

namespace UnicaenIndicateur\Controller;

use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenIndicateur\Entity\Db\TableauDeBord;
use UnicaenIndicateur\Form\TableauDeBord\TableauDeBordFormAwareTrait;
use UnicaenIndicateur\Service\Indicateur\IndicateurServiceAwareTrait;
use UnicaenIndicateur\Service\TableauDeBord\TableauDeBordServiceAwareTrait;

class TableauDeBordController extends AbstractActionController
{
    use IndicateurServiceAwareTrait;
    use TableauDeBordServiceAwareTrait;
    use TableauDeBordFormAwareTrait;

    public function indexAction() : ViewModel
    {
        $tableaux = $this->getTableauDeBordService()->getTableauxdeBord();

        return new ViewModel([
            'tableaux' => $tableaux,
        ]);
    }

    public function afficherAction() : ViewModel
    {
        $tableau = $this->getTableauDeBordService()->getRequestedTableauDeBord($this);
        $retour = $this->params()->fromQuery('retour');

        return new ViewModel([
            'tableau' => $tableau,
            'indicateurService' => $this->getIndicateurService(),
            'retour' => $retour,
        ]);
    }

    public function ajouterAction() : ViewModel
    {
        $tableau = new TableauDeBord();
        $namespace = $this->params()->fromQuery('namespace');

        $form = $this->getTableauDeBordForm();
        $form->setAttribute('action', $this->url()->fromRoute('tableau-de-bord/ajouter', [], ['query' => ['namespace' => $namespace]], true));
        if ($namespace) {
            $tableau->setNamespace($namespace);
            $form->get('namespace')->setAttribute('readonly', true);
        }
        $form->bind($tableau);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);

            if ($form->isValid()) {
                $this->getTableauDeBordService()->create($tableau);
            }
        }

        $vm = new ViewModel([
            'title' => "Création d'un tableau de bord",
            'form' => $form,
        ]);
        $vm->setTemplate('unicaen-indicateur/default/default-form');
        return $vm;
    }

    public function modifierAction() : ViewModel
    {
        $tableau = $this->getTableauDeBordService()->getRequestedTableauDeBord($this);
        $namespace = $this->params()->fromQuery('namespace');

        $form = $this->getTableauDeBordForm();
        $form->setAttribute('action', $this->url()->fromRoute('tableau-de-bord/modifier', ['tableau-de-bord' => $tableau->getId()], ['query' => ['namespace' => $namespace]], true));
        if ($namespace) {
            $tableau->setNamespace($namespace);
            $form->get('namespace')->setAttribute('readonly', true);
        }
        $form->bind($tableau);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getTableauDeBordService()->update($tableau);
            }
        }

        $vm = new ViewModel([
            'title' => "Modification des informations du tableau de bord",
            'form' => $form,
        ]);
        $vm->setTemplate('unicaen-indicateur/default/default-form');
        return $vm;
    }

    public function supprimerAction() : ViewModel
    {
        $tableau = $this->getTableauDeBordService()->getRequestedTableauDeBord($this);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") $this->getTableauDeBordService()->delete($tableau);
            exit();
        }

        $vm = new ViewModel();
        if ($tableau !== null) {
            $vm->setTemplate('application/default/confirmation');
            $vm->setVariables([
                'title' => "Suppression du tableau de bord [" . $tableau->getTitre() . "]",
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('tableau-de-bord/supprimer', ["tableau-de-bord" => $tableau->getId()], [], true),
            ]);
        }
        return $vm;
    }

    /** Gestion des indicateurs  ****************************************************************/

    public function ajouterIndicateurAction() : ViewModel
    {
        $tableau = $this->getTableauDeBordService()->getRequestedTableauDeBord($this);
        $indicateurs = $this->getIndicateurService()->getIndicateurs();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();

            $indicateur = $this->getIndicateurService()->getIndicateur($data['indicateur']);
            if ($indicateur) {
                $tableau->addIndicateur($indicateur);
                $this->getTableauDeBordService()->update($tableau);
            }
        }

        return new ViewModel([
            'title' => "Ajout d'un indicateur au tableau de bord",
            'tableau' => $tableau,
            'indicateurs' => $indicateurs,
        ]);
    }

    public function retirerIndicateurAction() : Response
    {
        $tableau = $this->getTableauDeBordService()->getRequestedTableauDeBord($this);
        $indicateur = $this->getIndicateurService()->getRequestedIndicateur($this);

        $tableau->removeIndicateur($indicateur);
        $this->getTableauDeBordService()->update($tableau);

        return $this->redirect()->toRoute('tableau-de-bord/afficher', ['tableau-de-bord' => $tableau->getId()], [], true);
    }
}