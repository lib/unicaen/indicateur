<?php

namespace UnicaenIndicateur\Controller;

use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenIndicateur\Entity\Db\Abonnement;
use UnicaenIndicateur\Service\Abonnement\AbonnementServiceAwareTrait;
use UnicaenIndicateur\Service\Indicateur\IndicateurServiceAwareTrait;
use UnicaenUtilisateur\Service\User\UserServiceAwareTrait;

class AbonnementController extends AbstractActionController {
    use AbonnementServiceAwareTrait;
    use IndicateurServiceAwareTrait;
    use UserServiceAwareTrait;

    public function indexAction() : ViewModel
    {
        $abonnements = [];

        return new ViewModel([
            'abonnements' => $abonnements,
        ]);
    }

    public function souscrireAction(): Response
    {
        $indicateur = $this->getIndicateurService()->getRequestedIndicateur($this);
        $user = $this->getUserService()->getConnectedUser();

        $abonnement = new Abonnement();
        $abonnement->setIndicateur($indicateur);
        $abonnement->setUser($user);
        $abonnement->setFrequence('P1D');
        $this->getAbonnementService()->create($abonnement);

        return $this->redirect()->toRoute('indicateurs', [], [], true);
    }

    public function resilierAction(): Response
    {
        $indicateur = $this->getIndicateurService()->getRequestedIndicateur($this);
        $user = $this->getUserService()->getConnectedUser();
        $retour = $this->params()->fromQuery('retour');

        $abonnements = $this->getAbonnementService()->getAbonnementsByUserAndIndicateur($user, $indicateur);
        foreach ($abonnements as $abonnement) $this->getAbonnementService()->delete($abonnement);

        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('indicateurs', [], [], true);
    }

    public function notifierAction(): Response
    {
        $this->getAbonnementService()->notifyAbonnements();
        return $this->redirect()->toRoute('indicateurs', [], [], true);
    }

}