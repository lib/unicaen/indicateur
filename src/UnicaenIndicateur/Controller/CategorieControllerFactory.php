<?php

namespace UnicaenIndicateur\Controller;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenIndicateur\Form\Categorie\CategorieForm;
use UnicaenIndicateur\Service\Categorie\CategorieService;

class CategorieControllerFactory {

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): CategorieController
    {
        /**
         * @var CategorieService $categorieService
         * @var CategorieForm $categorieForm
         */
        $categorieService = $container->get(CategorieService::class);
        $categorieForm = $container->get('FormElementManager')->get(CategorieForm::class);

        $controller = new CategorieController();
        $controller->setCategorieService($categorieService);
        $controller->setCategorieForm($categorieForm);
        return $controller;

    }
}