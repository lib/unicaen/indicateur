<?php

namespace UnicaenIndicateur\Controller;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenIndicateur\Service\Abonnement\AbonnementService;
use UnicaenIndicateur\Service\Indicateur\IndicateurService;
use UnicaenUtilisateur\Service\User\UserService;

;

class AbonnementControllerFactory
{

    /**
     * @param ContainerInterface $container
     * @return AbonnementController
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): AbonnementController
    {
        /**
         * @var AbonnementService $abonnementService
         * @var IndicateurService $indicateurService
         * @var UserService $userService
         */
        $abonnementService = $container->get(AbonnementService::class);
        $indicateurService = $container->get(IndicateurService::class);
        $userService = $container->get(UserService::class);

        $controller = new AbonnementController();
        $controller->setAbonnementService($abonnementService);
        $controller->setIndicateurService($indicateurService);
        $controller->setUserService($userService);
        return $controller;
    }
}