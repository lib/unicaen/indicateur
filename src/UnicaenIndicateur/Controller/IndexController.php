<?php

namespace UnicaenIndicateur\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenIndicateur\Entity\Db\Abonnement;
use UnicaenIndicateur\Service\Abonnement\AbonnementServiceAwareTrait;
use UnicaenIndicateur\Service\Indicateur\IndicateurServiceAwareTrait;
use UnicaenUtilisateur\Service\User\UserServiceAwareTrait;

class IndexController extends AbstractActionController {
    use AbonnementServiceAwareTrait;
    use IndicateurServiceAwareTrait;
    use UserServiceAwareTrait;

    public function indexAction() : ViewModel
    {
        $user = $this->getUserService()->getConnectedUser();
        $abonnements = $this->getAbonnementService()->getAbonnementsByUser($user);

        $result = [];
        foreach ($abonnements as $abonnement) {
            $indicateur = $abonnement->getIndicateur();
            $data = $this->getIndicateurService()->getIndicateurData($indicateur);
            $result[$indicateur->getId()] = count($data[1]);
        }

        return new ViewModel([
            'abonnements' => $abonnements,
            'result' => $result,
        ]);
    }

    public function abonnementAction() : ViewModel
    {
        $user = $this->getUserService()->getConnectedUser();
        $indicateurs = $this->getIndicateurService()->getIndicateurs();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();

            $id = $data['indicateur'];
            $indicateur = $this->getIndicateurService()->getIndicateur($id);

            if (!$this->getAbonnementService()->isAbonner($user, $indicateur)) {
                $abonnement = new Abonnement();
                $abonnement->setUser($user);
                $abonnement->setIndicateur($indicateur);
                $this->getAbonnementService()->create( $abonnement );
            }
        }

        return new ViewModel([
            'title' => "S'abonner à un indicateur",
            'indicateurs' => $indicateurs,
        ]);
    }

}