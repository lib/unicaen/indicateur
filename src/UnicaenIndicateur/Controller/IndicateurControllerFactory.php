<?php

namespace UnicaenIndicateur\Controller;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenIndicateur\Form\Indicateur\IndicateurForm;
use UnicaenIndicateur\Service\Abonnement\AbonnementService;
use UnicaenIndicateur\Service\Indicateur\IndicateurService;
use Interop\Container\ContainerInterface;
use UnicaenIndicateur\Service\Perimetre\PerimetreServiceInterface;
use UnicaenMail\Service\Mail\MailService;
use UnicaenUtilisateur\Service\User\UserService;

class IndicateurControllerFactory {

    /**
     * @param ContainerInterface $container
     * @return IndicateurController
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : IndicateurController
    {
        /**
         * @var AbonnementService $abonnementService
         * @var IndicateurService $indicateurService
         * @var MailService $mailService
         * @var UserService $userService
         * @var PerimetreServiceInterface $perimetreService
         */
        $abonnementService = $container->get(AbonnementService::class);
        $indicateurService = $container->get(IndicateurService::class);
        $mailService = $container->get(MailService::class);
        $userService = $container->get(UserService::class);

        $config = $container->get('config');
        $perimetreServiceKey = $container->get($config['unicaen-indicateur']['perimetreService']);
        /** @var PerimetreServiceInterface $perimetreService */
        $perimetreService = $container->get($perimetreServiceKey::class);

        /**
         * @var IndicateurForm $indicateurForm
         */
        $indicateurForm = $container->get('FormElementManager')->get(IndicateurForm::class);

        $controller = new IndicateurController();
        $controller->setAbonnementService($abonnementService);
        $controller->setIndicateurService($indicateurService);
        $controller->setMailService($mailService);
        $controller->setPerimetreService($perimetreService);
        $controller->setUserService($userService);
        $controller->setIndicateurForm($indicateurForm);
        return $controller;
    }
}