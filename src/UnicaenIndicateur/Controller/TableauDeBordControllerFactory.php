<?php

namespace UnicaenIndicateur\Controller;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenIndicateur\Entity\Db\TableauDeBord;
use UnicaenIndicateur\Form\TableauDeBord\TableauDeBordForm;
use UnicaenIndicateur\Service\Indicateur\IndicateurService;
use UnicaenIndicateur\Service\TableauDeBord\TableauDeBordService;

class TableauDeBordControllerFactory {

    /**
     * @param ContainerInterface $container
     * @return TableauDeBordController
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : TableauDeBordController
    {
        /**
         * @var IndicateurService $indicateurService
         * @var TableauDeBordService $tableauService
         * @var TableauDeBordForm $tableauForm
         */
        $indicateurService = $container->get(IndicateurService::class);
        $tableauService = $container->get(TableauDeBordService::class);
        $tableauForm = $container->get('FormElementManager')->get(TableauDeBordForm::class);

        $controller = new TableauDeBordController();
        $controller->setIndicateurService($indicateurService);
        $controller->setTableauDeBordForm($tableauForm);
        $controller->setTableauDeBordService($tableauService);
        return $controller;
    }
}