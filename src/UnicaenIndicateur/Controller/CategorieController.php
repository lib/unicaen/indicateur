<?php

namespace UnicaenIndicateur\Controller;

use Laminas\Http\Request;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenIndicateur\Entity\Db\Categorie;
use UnicaenIndicateur\Form\Categorie\CategorieFormAwareTrait;
use UnicaenIndicateur\Service\Categorie\CategorieServiceAwareTrait;

class CategorieController extends AbstractActionController
{
    use CategorieServiceAwareTrait;
    use CategorieFormAwareTrait;

    public function indexAction(): ViewModel
    {
        $categories  = $this->getCategorieService()->getCategories();

        return new ViewModel([
            'categories' => $categories,
        ]);
    }

    public function afficherAction(): ViewModel
    {
        $categorie  = $this->getCategorieService()->getRequestedCategorie($this);

        return new ViewModel([
            'categorie' => $categorie,
        ]);
    }


    public function ajouterAction(): ViewModel
    {
        $categorie = new Categorie();
        $form = $this->getCategorieForm();
        $form->setAttribute('action', $this->url()->fromRoute('indicateur/categorie/ajouter', [], [], true));
        $form->bind($categorie);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getCategorieService()->create($categorie);
                exit();
            }
        }

        $vm = new ViewModel([
            'title' => "Ajout d'une catégorie d'indicateur",
            'form' => $form,
        ]);
        $vm->setTemplate('unicaen-indicateur/default/default-form');
        return $vm;
    }

    public function modifierAction(): ViewModel
    {
        $categorie  = $this->getCategorieService()->getRequestedCategorie($this);

        $form = $this->getCategorieForm();
        $form->setAttribute('action', $this->url()->fromRoute('indicateur/categorie/modifier', ['categorie' => $categorie->getId()], [], true));
        $form->bind($categorie);
        $form->setOldCode($categorie->getCode());

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getCategorieService()->update($categorie);
                exit();
            }
        }

        $vm = new ViewModel([
            'title' => "Modification d'une catégorie d'indicateur",
            'form' => $form,
        ]);
        $vm->setTemplate('unicaen-indicateur/default/default-form');
        return $vm;
    }

    public function supprimerAction(): ViewModel
    {
        $categorie  = $this->getCategorieService()->getRequestedCategorie($this);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") {
                $this->getCategorieService()->delete($categorie);
            }
            exit();
        }

        $vm = new ViewModel();
        if ($categorie !== null) {
            $vm->setTemplate('unicaen-indicateur/default/confirmation');
            $vm->setVariables([
                'title' => "Suppression de la catégorie " . $categorie->getLibelle(),
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('indicateur/categorie/supprimer', ["categorie" => $categorie->getId()], [], true),
            ]);
        }
        return $vm;
    }
}