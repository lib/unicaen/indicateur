<?php

namespace UnicaenIndicateur\Entity\Db;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Indicateur {

    const ENTITY_AGENT = 'Agent';
    const ENTITY_STRUCTURE = 'Structure';
    const ENTITY_LIBRE = 'Libre';
    const ENTITY_ADAPTATIF = 'Adaptatif';

    private ?int $id = null;
    private ?string $code = null;
    private ?string $titre = null;
    private ?string $description = null;
    private ?Categorie  $categorie = null;

    private ?string $requete = null;
    private ?DateTime $dernierRafraichissement = null;
    private ?string $viewId = null;
    private ?string $entity = null;
    private ?string $namespace = null;
    private Collection $abonnements;
    private Collection $tableaux;
    private ?int $nbElements = null;

    private ?string $perimetre = null;

    public function __construct()
    {
        $this->abonnements = new ArrayCollection();
        $this->tableaux = new ArrayCollection();
    }

    public function getId() : ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    public function getTitre() : ?string
    {
        return $this->titre;
    }

    public function setTitre(?string $titre) : void
    {
        $this->titre = $titre;
    }

    public function getDescription() : ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description) : void
    {
        $this->description = $description;
    }

    public function getCategorie(): ?Categorie
    {
        return $this->categorie;
    }

    public function setCategorie(?Categorie $categorie): void
    {
        $this->categorie = $categorie;
    }



    public function getRequete() : ?string
    {
        return $this->requete;
    }

    public function setRequete(?string $requete) : void
    {
        $this->requete = $requete;
    }

    public function getDernierRafraichissement() : ?DateTime
    {
        return $this->dernierRafraichissement;
    }

    public function setDernierRafraichissement(?DateTime $dernierRafraichissement) : void
    {
        $this->dernierRafraichissement = $dernierRafraichissement;
    }

    public function getViewId() : ?string
    {
        return $this->viewId;
    }

    public function setViewId(?string $viewId) : void
    {
        $this->viewId = $viewId;
    }

    public function getEntity() : ?string
    {
        return $this->entity;
    }

    public function setEntity(?string $entity) : void
    {
        $this->entity = $entity;
    }

    public function getNamespace(): ?string
    {
        return $this->namespace;
    }

    public function setNamespace(?string $namespace): void
    {
        $this->namespace = $namespace;
    }

    public function getNbElements(): ?int
    {
        return $this->nbElements;
    }

    public function setNbElements(?int $nbElements): void
    {
        $this->nbElements = $nbElements;
    }

    public function getPerimetre(): ?string
    {
        return $this->perimetre;
    }

    public function setPerimetre(?string $perimetre): void
    {
        $this->perimetre = $perimetre;
    }


    /** Abonnements **************************************************************** */

    /**
     * @return Abonnement[]
     */
    public function getAbonnements() : array
    {
        return $this->abonnements->toArray();
    }

    public function addAbonnement(Abonnement $abonnement) : void
    {
        $this->abonnements->add($abonnement);
    }

    public function removeAbonnement(Abonnement $abonnement) : void
    {
        $this->abonnements->removeElement($abonnement);
    }

    public function hasAbonnement(Abonnement $abonnement) : bool
    {
        return $this->abonnements->contains($abonnement);
    }

    /** Tableau *****************************************************************/

    /**
     * @return TableauDeBord[]
     */
    public function getTableauxDeBord() : array
    {
        return $this->tableaux->toArray();
    }
    
    /** MACRO **********************************************************************/

    /** @noinspection  PhpUnused */
    public function generateTag() : string
    {
        return 'Indicateur_' . $this->getId();
    }


}