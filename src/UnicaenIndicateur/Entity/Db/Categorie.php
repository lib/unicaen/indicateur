<?php

namespace UnicaenIndicateur\Entity\Db;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Categorie {

    private ?int $id = null;
    private ?string $code = null;
    private ?string $libelle = null;
    private ?string $description = null;
    private ?int $ordre = 9999;

    private Collection $indicateurs;


    public function __construct()
    {
        $this->indicateurs = new ArrayCollection();
    }

    public function getId() : ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): void
    {
        $this->libelle = $libelle;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getOrdre(): ?int
    {
        return $this->ordre;
    }

    public function setOrdre(?int $ordre): void
    {
        $this->ordre = $ordre;
    }

    public function getIndicateurs(): Collection
    {
        return $this->indicateurs;
    }

    public function setIndicateurs(Collection $indicateurs): void
    {
        $this->indicateurs = $indicateurs;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

}