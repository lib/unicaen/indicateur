<?php

namespace UnicaenIndicateur\Entity\Db;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class TableauDeBord {

    private ?int $id = null;
    private ?string $titre = null;
    private ?string $description = null;
    private ?int $nbColumn = 1;
    private ?string $namespace = null;
    private Collection $indicateurs;

    public function __construct()
    {
        $this->indicateurs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(?string $titre): void
    {
        $this->titre = $titre;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getNbColumn(): ?int
    {
        return $this->nbColumn;
    }

    public function setNbColumn(?int $nbColumn): void
    {
        $this->nbColumn = $nbColumn;
    }

    public function getNamespace(): ?string
    {
        return $this->namespace;
    }

    public function setNamespace(?string $namespace): void
    {
        $this->namespace = $namespace;
    }

    /**
     * @return Indicateur[]
     */
    public function getIndicateurs(): array
    {
        $array = $this->indicateurs->toArray();
        return $array;
    }

    public function addIndicateur(Indicateur $indicateur) : void
    {
        $this->indicateurs->add($indicateur);
    }

    public function removeIndicateur(Indicateur $indicateur) : void
    {
        $this->indicateurs->removeElement($indicateur);
    }

    public function hasIndicateur(Indicateur $indicateur) : bool
    {
        return $this->indicateurs->contains($indicateur);
    }
}