<?php

namespace UnicaenIndicateur\Entity\Db;

use DateTime;
use UnicaenUtilisateur\Entity\Db\User;

class Abonnement {

    private ?int $id = null;
    private ?User $user = null;
    private ?Indicateur $indicateur = null;
    private ?string $frequence = null;
    private ?DateTime $dernierEnvoi = null;

    public function getId() : ?int
    {
        return $this->id;
    }

    public function getUser() : ?User
    {
        return $this->user;
    }

    public function setUser(?User $user) : void
    {
        $this->user = $user;
    }

    public function getIndicateur() : ?Indicateur
    {
        return $this->indicateur;
    }

    public function setIndicateur(?Indicateur $indicateur) : void
    {
        $this->indicateur = $indicateur;
    }

    public function getFrequence() : ?string
    {
        return $this->frequence;
    }

    public function setFrequence(?string $frequence) : void
    {
        $this->frequence = $frequence;
    }

    public function getDernierEnvoi() : ?DateTime
    {
        return $this->dernierEnvoi;
    }

    public function setDernierEnvoi(?DateTime $denierEnvoi) : void
    {
        $this->dernierEnvoi = $denierEnvoi;
    }


}