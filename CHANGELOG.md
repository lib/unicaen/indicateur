#Version 6.2.1 21/01/2025

* Déclaration des périmètres en configuration
* Périmètres multiples

/!\ la colonne `perimetre` dans la table `unicaen_indicateur_indicateur` a évolué /!\
```sql
alter table unicaen_indicateur_indicateur alter column perimetre drop not null;
alter table unicaen_indicateur_indicateur alter column perimetre drop default;
```

#Version 6.2.0 17/01/2025

* Intégration d'une première version des périmétres 

/!\ nouvelle colonne `perimetre` dans la table `unicaen_indicateur_indicateur` /!\
```sql
alter table unicaen_indicateur_indicateur add perimetre varchar(256) default 'aucun' not null;
```
