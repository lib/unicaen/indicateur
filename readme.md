Module Unicaen Indicateur : `unicaen/indicateur`
==========================
----

# Périmètres

## Principe des périmètres

La notion de périmètres permet de filtrer les résultats d'un indicateur en fonction de ceux-ci.
Ce qui permet de ne présenter des résultats que dans le périmètre d'action d'un User/Role.

Exemple :
```markdown
Pour un indicateur présentant des inscriptions à des formations pour lequel on précise le périmètre de structure des inscrit·es.
Alors, on pourra filtrer les données en fonction des structures associées aux responsables.

Si on expose un périmètre de domaine des formations sur ce même indicateur.
Alors, on pourra filtrer les données pour les référents selon les domaines dont ils sont référents
```

## Périmètres implementés/hardcodés

## Déclaration de périmètres

Les périmètres que l'application exploite sont à déclarer en configuration. 
Le tableau de déclaration contient comme clef le nom du périmètre et en valeur une description qui sera présentée dans le formulaire de création des indicateurs.

peut être déclarer dans le fichier [unicaen-indicateur.global.php](config/unicaen-indicateur.global.php.dist)
```php
return [
    'unicaen-indicateur' => [
        'role' => "Affichage restreint aux rôles de l'usagers",  
        'structure' => "Affichage restreint aux structures de l'usagers",  
    ],
];
```

**Attention** :  l'usage d'un périmètre suppose la déclatation d'une colonne perimetre_NOM_id dans l'indicateur qui sera exploité par le filtrage.
Ainsi pour le périmetre `structure` la colonne de filtrage sera nommée `perimetre_structure_id`.

**N.B.** : Les colonnes de travail seront masquées des affichages et exportations.

## Récupération des périmètres depuis l'application maîtresse

Dans la configuration de la biliothèque, il est nécessaire de déclarer le chemin vers un service en charge de renvoyer les périmètres.

peut être déclarer dans le fichier [unicaen-indicateur.global.php](config/unicaen-indicateur.global.php.dist)
```php
return [
    'unicaen-indicateur' => [
            'perimetreService' => PerimetreService::class,
    ],
];
```
Cette classe devra implentée l'interface [PerimetreServiceInterface](src/UnicaenIndicateur/Service/Perimetre/PerimetreServiceInterface.php).
La fonction getPerimetres dans le contrat de l'interface prend en paramètres un utilisateur `AbstractUser` et  un rôle `AbstractRole` et retour un tabeau de chaine de caractères contenant les périmètres.

Par exemple pour les périmètres de structure 5 et 12 et de domaine 1 cette méthode retourne :
```php
['STRUCTURE_5','STRUCTURE_12', 'DOMAINE_1']
```

Améliorations
-------------
1. **SECURITE**  Verifier qu'il n'y est que des selects
1. **QoL**  Coloration tinyMCE de la requete SQL